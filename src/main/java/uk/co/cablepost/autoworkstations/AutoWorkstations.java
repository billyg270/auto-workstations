package uk.co.cablepost.autoworkstations;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandRegister;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneBlock;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneRegister;

public class AutoWorkstations implements ModInitializer {
    public static final String MOD_ID = "autoworkstations";

    public static final float EXPERIENCE_BOTTLE_VALUE = 8.0f;

    public static final ItemGroup ITEM_GROUP = FabricItemGroup.builder()
            .icon(() -> new ItemStack(AutoCraftingTableRegister.GOLD_AUTO_CRAFTING_TABLE_BLOCK))
                    .displayName(Text.translatable("itemGroup.autoworkstations.items"))
            .entries((context, entries) -> {
                entries.add(AutoCraftingTableRegister.IRON_AUTO_CRAFTING_TABLE_BLOCK);
                entries.add(AutoCraftingTableRegister.GOLD_AUTO_CRAFTING_TABLE_BLOCK);
                entries.add(AutoCraftingTableRegister.NETHERITE_AUTO_CRAFTING_TABLE_BLOCK);
                entries.add(AutoFurnaceRegister.IRON_AUTO_FURNACE_BLOCK);
                entries.add(AutoFurnaceRegister.GOLD_AUTO_FURNACE_BLOCK);
                entries.add(AutoEnchantingTableRegister.IRON_AUTO_ENCHANTING_TABLE_BLOCK);
                entries.add(AutoEnchantingTableRegister.GOLD_AUTO_ENCHANTING_TABLE_BLOCK);
                entries.add(AutoExperienceOrbVacuumRegister.IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
                entries.add(AutoExperienceOrbVacuumRegister.GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
                entries.add(AutoExperienceOrbEmitterRegister.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
                entries.add(AutoExperienceOrbEmitterRegister.GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
                entries.add(AutoAnvilRegister.IRON_AUTO_ANVIL_BLOCK);
                entries.add(AutoAnvilRegister.GOLD_AUTO_ANVIL_BLOCK);
                entries.add(AutoBrewingStandRegister.IRON_AUTO_BREWING_STAND_BLOCK);
                entries.add(AutoBrewingStandRegister.GOLD_AUTO_BREWING_STAND_BLOCK);

                entries.add(AutoGrindstoneRegister.IRON_AUTO_GRINDSTONE_BLOCK);
                entries.add(AutoGrindstoneRegister.GOLD_AUTO_GRINDSTONE_BLOCK);
            })
            .build()
    ;

    @Override
    public void onInitialize() {
        AutoCraftingTableRegister.onInitialize();
        AutoFurnaceRegister.onInitialize();
        AutoEnchantingTableRegister.onInitialize();
        AutoExperienceOrbVacuumRegister.onInitialize();
        AutoExperienceOrbEmitterRegister.onInitialize();
        AutoAnvilRegister.onInitialize();
        AutoBrewingStandRegister.onInitialize();

        AutoGrindstoneRegister.onInitialize();

        Registry.register(Registries.ITEM_GROUP, Identifier.of(MOD_ID, "main_creative_inventory_tab"), ITEM_GROUP);
    }
}
