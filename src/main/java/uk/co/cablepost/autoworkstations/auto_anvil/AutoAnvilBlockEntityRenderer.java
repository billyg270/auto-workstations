package uk.co.cablepost.autoworkstations.auto_anvil;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import org.joml.Vector3f;
import net.minecraft.world.World;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlockEntity;
import net.minecraft.util.math.RotationAxis;

import java.util.Objects;

public class AutoAnvilBlockEntityRenderer<T extends AutoAnvilBlockEntity> implements BlockEntityRenderer<T> {

    public AutoAnvilBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
    }

    @Override
    public void render(AutoAnvilBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        matrices.push();

        World world = blockEntity.getWorld();

        int worldLight = 0;
        if(world != null) {
            worldLight = Math.max(
                    Math.max(
                        WorldRenderer.getLightmapCoordinates(world, blockEntity.getPos().north()),
                        WorldRenderer.getLightmapCoordinates(world, blockEntity.getPos().south())
                    ),
                    Math.max(
                        Math.max(
                            WorldRenderer.getLightmapCoordinates(world, blockEntity.getPos().east()),
                            WorldRenderer.getLightmapCoordinates(world, blockEntity.getPos().west())
                        ),
                        WorldRenderer.getLightmapCoordinates(world, blockEntity.getPos().up())
                    )
            );
        }

        ItemStack anvilStack = blockEntity.getStack(AutoAnvilBlockEntity.SLOT_ANVIL);

        if(
                !anvilStack.isEmpty() &&
                (
                        anvilStack.isOf(Items.ANVIL) ||
                        anvilStack.isOf(Items.CHIPPED_ANVIL) ||
                        anvilStack.isOf(Items.DAMAGED_ANVIL)
                )
        ){
            matrices.translate(0.5f, -0.145f, 0.5f);
            matrices.scale(2.6f, 2.4f, 2.6f);
            MinecraftClient.getInstance().getItemRenderer().renderItem(anvilStack, ModelTransformationMode.GROUND, worldLight, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
            matrices.scale(1f/2.6f, 1f/2.4f, 1f/2.6f);
            matrices.translate(-0.5f, 0.145f, -0.5f);

            ItemStack inStack = blockEntity.getStack(AutoAnvilBlockEntity.SLOT_ITEM_IN);
            ItemStack addStack = blockEntity.getStack(AutoAnvilBlockEntity.SLOT_ITEM_ADD);

            if(!inStack.isEmpty()){
                renderItem(matrices, vertexConsumers, worldLight, overlay, inStack, new Vector3f(0.5f, 0.53f, 0.5f - 0.1f), blockEntity.getWorld());
            }

            if(!addStack.isEmpty()){
                renderItem(matrices, vertexConsumers, worldLight, overlay, addStack, new Vector3f(0.5f, 0.53f, 0.5f + 0.1f), blockEntity.getWorld());
            }
        }

        ItemStack hammer_handle = new ItemStack(Items.DARK_OAK_PLANKS);
        ItemStack hammer_head = new ItemStack(Items.IRON_BLOCK);

        matrices.translate(0.12f, 0.75f, 0.5f);

        matrices.scale(0.05f, 0.05f, 0.05f);

        float angle = 0f;//Math.sin(blockEntity  blockEntity.processProgress) * 30;

        if(blockEntity instanceof IronAutoAnvilBlockEntity) {
            if (blockEntity.processProgress >= 43) {
                angle = (float) (Math.sin(blockEntity.processProgress * 0.9f + 1f) * 20f);
            }
        }
        if(blockEntity instanceof GoldAutoAnvilBlockEntity) {
            if (blockEntity.processProgress >= 10) {
                angle = (float) (Math.sin(blockEntity.processProgress * 0.9f + 4f) * 20f);
            }
        }

        matrices.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(angle));

        matrices.translate(3f, 0f, 0f);
        matrices.scale(8f, 1f, 1f);

        MinecraftClient.getInstance().getItemRenderer().renderItem(hammer_handle, ModelTransformationMode.NONE, worldLight, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);

        matrices.scale(1/8f * 2.5f, 3f, 3f);
        matrices.translate(1.8f, 0f, 0f);

        MinecraftClient.getInstance().getItemRenderer().renderItem(hammer_head, ModelTransformationMode.NONE, worldLight, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);

        matrices.pop();
    }

    public void renderItem(MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay, ItemStack itemStack, Vector3f pos, World world){
        boolean hasDepth = Objects.requireNonNull(MinecraftClient.getInstance().getItemRenderer().getModels().getModel(itemStack.getItem())).hasDepth();

        if(!hasDepth){
            pos.add(0f, 0.08f, 0.05f);
        }
        else{
            pos.add(0f, 0.05f, 0.0f);
        }

        float itemScale = 0.4f;

        matrices.translate(pos.x(), pos.y(), pos.z());

        matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(180));

        if(!hasDepth){
            matrices.multiply(RotationAxis.POSITIVE_X.rotationDegrees(90));
        }

        matrices.scale(itemScale, itemScale, itemScale);

        MinecraftClient.getInstance().getItemRenderer().renderItem(itemStack, ModelTransformationMode.GROUND, light, overlay, matrices, vertexConsumers, world, 0);


        matrices.scale(1/itemScale, 1/itemScale, 1/itemScale);

        if(!hasDepth){
            matrices.multiply(RotationAxis.POSITIVE_X.rotationDegrees(-90));
        }

        matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(-180));

        matrices.translate(-pos.x(), -pos.y(), -pos.z());
    }
}
