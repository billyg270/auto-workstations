package uk.co.cablepost.autoworkstations.auto_anvil;

import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlockEntity;

public class AutoAnvilRegister {

    public static final CustomPayload.Id<UpdateAnvilModePayload> UPDATE_AUTO_ANVIL_PACKET_ID = new CustomPayload.Id<>(Identifier.of(AutoWorkstations.MOD_ID, "update_auto_anvil"));

    //screen
    public static final Identifier AUTO_ANVIL_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_anvil");
    public static final ScreenHandlerType<AutoAnvilScreenHandler> AUTO_ANVIL_SCREEN_HANDLER = new ScreenHandlerType<>(AutoAnvilScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoAnvilBlock IRON_AUTO_ANVIL_BLOCK = new IronAutoAnvilBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.ANVIL).strength(2.0f));
    public static final Identifier IRON_AUTO_ANVIL_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_anvil");
    public static BlockEntityType<IronAutoAnvilBlockEntity> IRON_AUTO_ANVIL_BLOCK_ENTITY;

    //gold
    public static AutoAnvilBlock GOLD_AUTO_ANVIL_BLOCK = new GoldAutoAnvilBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.ANVIL).strength(1.0f));
    public static final Identifier GOLD_AUTO_ANVIL_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_anvil");
    public static BlockEntityType<GoldAutoAnvilBlockEntity> GOLD_AUTO_ANVIL_BLOCK_ENTITY;

    public static void onInitialize(){
        PayloadTypeRegistry.playC2S().register(UPDATE_AUTO_ANVIL_PACKET_ID, UpdateAnvilModePayload.CODEC);
        //iron
        IRON_AUTO_ANVIL_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_ANVIL_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoAnvilBlockEntity::new,
                        IRON_AUTO_ANVIL_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_ANVIL_IDENTIFIER, IRON_AUTO_ANVIL_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_ANVIL_IDENTIFIER, new BlockItem(IRON_AUTO_ANVIL_BLOCK, new Item.Settings()));

        //gold
        GOLD_AUTO_ANVIL_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_ANVIL_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoAnvilBlockEntity::new,
                        GOLD_AUTO_ANVIL_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_ANVIL_IDENTIFIER, GOLD_AUTO_ANVIL_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_ANVIL_IDENTIFIER, new BlockItem(GOLD_AUTO_ANVIL_BLOCK, new Item.Settings()));

        ServerPlayNetworking.registerGlobalReceiver(AutoAnvilRegister.UPDATE_AUTO_ANVIL_PACKET_ID, (payload, context) -> {
            int anvilMode = payload.mode();

            PlayerEntity player = context.player();
            if (!player.isSpectator() && player.currentScreenHandler instanceof AutoAnvilScreenHandler screenHandler) {
                screenHandler.setSelectedMode(anvilMode);
            }
        });

        Registry.register(Registries.SCREEN_HANDLER, AUTO_ANVIL_IDENTIFIER, AUTO_ANVIL_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        BlockEntityRendererRegistry.register(IRON_AUTO_ANVIL_BLOCK_ENTITY, AutoAnvilBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(GOLD_AUTO_ANVIL_BLOCK_ENTITY, AutoAnvilBlockEntityRenderer::new);

        HandledScreens.register(AUTO_ANVIL_SCREEN_HANDLER, AutoAnvilScreen::new);
    }

    public record UpdateAnvilModePayload(int mode) implements CustomPayload {
        public static final PacketCodec<RegistryByteBuf, UpdateAnvilModePayload> CODEC = CustomPayload.codecOf(UpdateAnvilModePayload::write, UpdateAnvilModePayload::fromBuf);

        private static UpdateAnvilModePayload fromBuf(RegistryByteBuf buf) {
            return new UpdateAnvilModePayload(buf.readVarInt());
        }

        private void write(RegistryByteBuf buf) {
            buf.writeVarInt(this.mode);
        }

        @Override
        public Id<? extends CustomPayload> getId() {
            return AutoAnvilRegister.UPDATE_AUTO_ANVIL_PACKET_ID;
        }
    }

}
