package uk.co.cablepost.autoworkstations.auto_anvil;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import java.util.List;

public class AutoAnvilScreen extends HandledScreen<AutoAnvilScreenHandler> {
    private static final Identifier TEXTURE = Identifier.of(AutoWorkstations.MOD_ID, "textures/gui/container/auto_anvil.png");

    public int lastXpEmptyProgress;

    public AutoAnvilScreen(AutoAnvilScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.titleY = 4;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //xp empty progress
        int xpEmptyProgress = handler.getXpEmptyProgress();
        context.drawTexture(TEXTURE, x + 5, y + 42, 176, 55, 18, xpEmptyProgress);

        //xp bar progress
        int targetExpProgress = handler.getExpProgress();
        int expProgress = targetExpProgress;
        if(targetExpProgress > lastXpEmptyProgress){
            expProgress = Math.min(lastXpEmptyProgress + 2, targetExpProgress);
        }
        context.drawTexture(TEXTURE, x + 37, y + 18 + (72 - expProgress), 180, 85, 6, expProgress);
        lastXpEmptyProgress = expProgress;

        //process progress
        int processProgress = handler.getProcessProgress();
        context.drawTexture(TEXTURE, x + 115, y + 47, 176, 14, processProgress + 1, 16);

        //Mode Select
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_anvil.mode.repair"), x + 69, y + 23, 4210752, false);
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_anvil.mode.rename"), x + 118, y + 23, 4210752, false);

        int mode = handler.getSelectedMode();
        if(mode == AutoAnvilBlockEntity.MODE_REPAIR){
            context.drawText(this.textRenderer, Text.of("x"), x + 60, y + 22, 0xffffff, false);
        }
        if(mode == AutoAnvilBlockEntity.MODE_RENAME){
            context.drawText(this.textRenderer, Text.of("x"), x + 109, y + 22, 0xffffff, false);
        }

        //xp level
        String xpLevelStr = "" + handler.getExpLevel();
        int xpLevelTxtStart = 33 - xpLevelStr.length() * 6;
        context.drawText(this.textRenderer, Text.of(xpLevelStr), x + xpLevelTxtStart, y + 47, 0x33de00, true);

        //required xp level
        int requiredXpLevel = handler.getLevelCost();
        String requiredXpLevelStr = requiredXpLevel >= 40 ? "Too Expensive" : ("" + requiredXpLevel);
        int requiredXpLevelTxtStart = 81 - requiredXpLevelStr.length() * 3;
        context.drawText(this.textRenderer, Text.of(requiredXpLevelStr), x + requiredXpLevelTxtStart, y + 34, requiredXpLevel >= 40 ? 0xEE3333 : 0x33de00, true);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(context, mouseX, mouseY);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if(client == null || client.player == null || client.player.isSpectator()){
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        if(mouseY >= y + 23 && mouseY <= y + 29) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                ClientPlayNetworking.send(new AutoAnvilRegister.UpdateAnvilModePayload(AutoAnvilBlockEntity.MODE_REPAIR));
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                ClientPlayNetworking.send(new AutoAnvilRegister.UpdateAnvilModePayload(AutoAnvilBlockEntity.MODE_RENAME));
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    protected void drawForeground(DrawContext context, int mouseX, int mouseY) {
        context.drawText(this.textRenderer, this.title, this.titleX, this.titleY, 4210752, false);

        if(mouseY >= y + 23 && mouseY <= y + 29) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                //Repair
                context.drawTooltip(
                        this.textRenderer,
                        Text.translatable("gui.autoworkstations.auto_anvil.mode.repair.tooltip"),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                //Rename
                context.drawTooltip(
                        this.textRenderer,
                        List.of(
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_1"),
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_2"),
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_3")
                        ),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
        }
    }
}
