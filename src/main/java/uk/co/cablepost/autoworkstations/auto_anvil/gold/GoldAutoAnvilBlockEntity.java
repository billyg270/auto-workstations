package uk.co.cablepost.autoworkstations.auto_anvil.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;

public class GoldAutoAnvilBlockEntity extends AutoAnvilBlockEntity {
    public GoldAutoAnvilBlockEntity(BlockPos pos, BlockState state) {
        super(AutoAnvilRegister.GOLD_AUTO_ANVIL_BLOCK_ENTITY, pos, state);
        xpEmptyBottleMaxProgress = 10;
        maxProcessProgress = 30;
    }
}
