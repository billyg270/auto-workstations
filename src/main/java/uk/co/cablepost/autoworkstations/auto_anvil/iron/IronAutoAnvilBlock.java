package uk.co.cablepost.autoworkstations.auto_anvil.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlock;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoAnvilBlock extends AutoAnvilBlock {

    public static final MapCodec<IronAutoAnvilBlock> CODEC = createCodec(IronAutoAnvilBlock::new);

    public IronAutoAnvilBlock(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoAnvilBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return
            world.isClient ?
            IronAutoAnvilBlock.validateTicker(type, AutoAnvilRegister.IRON_AUTO_ANVIL_BLOCK_ENTITY, IronAutoAnvilBlockEntity::clientTick) :
            IronAutoAnvilBlock.validateTicker(type, AutoAnvilRegister.IRON_AUTO_ANVIL_BLOCK_ENTITY, IronAutoAnvilBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_anvil.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
