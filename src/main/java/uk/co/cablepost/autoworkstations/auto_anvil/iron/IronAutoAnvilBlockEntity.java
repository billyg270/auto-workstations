package uk.co.cablepost.autoworkstations.auto_anvil.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;

public class IronAutoAnvilBlockEntity extends AutoAnvilBlockEntity {
    public IronAutoAnvilBlockEntity(BlockPos pos, BlockState state) {
        super(AutoAnvilRegister.IRON_AUTO_ANVIL_BLOCK_ENTITY, pos, state);
        xpEmptyBottleMaxProgress = 20;
        maxProcessProgress = 60;
    }
}
