package uk.co.cablepost.autoworkstations.auto_brewing_stand;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.BrewingStandBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.BrewingStandBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.recipe.BrewingRecipeRegistry;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoBrewingStandBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {

    public static int INVENTORY_SIZE = 8;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static int SLOT_BLAZE_POWDER = 0;
    public static int SLOT_INPUT = 1;
    public static int SLOT_POTION_A_START = 2;//potions in / being processed
    public static int SLOT_POTION_A_END = 4;
    public static int SLOT_POTION_B_START = 5;//potions done
    public static int SLOT_POTION_B_END = 7;

    int brewTime;
    int fuel;

    private Item itemBrewing;
    private boolean[] slotsEmptyLastTick;

    int brewTimeStart = 400;

    public AutoBrewingStandBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state, int brewTimeStart) {
        super(type, pos, state);
        this.brewTimeStart = brewTimeStart;
    }

    @Override
    public void readNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(nbt, wrapperLookup);

        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory, wrapperLookup);

        this.brewTime = nbt.getShort("BrewTime");
        this.fuel = nbt.getByte("Fuel");
    }

    @Override
    protected void writeNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.writeNbt(nbt, wrapperLookup);

        Inventories.writeNbt(nbt, inventory, wrapperLookup);

        nbt.putShort("BrewTime", (short)this.brewTime);
        nbt.putByte("Fuel", (byte)this.fuel);
    }

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0 -> {
                    return brewTime;
                }
                case 1 -> {
                    return fuel;
                }
                case 2 -> {
                    return brewTimeStart;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case 0 -> brewTime = value;
                case 1 -> fuel = value;
                case 2 -> brewTimeStart = value;
            }
        }

        @Override
        public int size() {
            return 3;
        }
    };

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoBrewingStandBlockEntity blockEntity) {

    }

    public static void serverTick(World world, BlockPos blockPos, BlockState blockState, AutoBrewingStandBlockEntity blockEntity) {
        // public static void tick(World world, BlockPos pos, BlockState state, BrewingStandBlockEntity blockEntity) {
        // BrewingStandBlockEntity.tick(world, blockPos, blockState, blockEntity);

        //Refuel

        ItemStack fuelStack = blockEntity.inventory.get(SLOT_BLAZE_POWDER);
        if (blockEntity.fuel <= 0 && fuelStack.isOf(Items.BLAZE_POWDER)) {
            blockEntity.fuel = 20;
            fuelStack.decrement(1);
            blockEntity.markDirty();
        }

        //Craft

        ItemStack inputStack = blockEntity.inventory.get(SLOT_INPUT);

        boolean canCraft = canCraft(blockEntity.inventory, world);

        if (blockEntity.brewTime > 0) {
            for(int i = 0; i <= 2; i++) {
                if (!blockEntity.inventory.get(SLOT_POTION_A_START + i).isEmpty() && !blockEntity.inventory.get(SLOT_POTION_B_START + i).isEmpty()) {
                    return;//won't be able to move finished potion into output slot below
                }
            }

            --blockEntity.brewTime;
            if (blockEntity.brewTime == 0 && canCraft) {
                AutoBrewingStandBlockEntity.craft(world, blockPos, blockEntity.inventory);

                for(int i = 0; i <= 2; i++){
                    if(!blockEntity.inventory.get(SLOT_POTION_A_START + i).isEmpty()){
                        blockEntity.inventory.set(SLOT_POTION_B_START + i, blockEntity.inventory.get(SLOT_POTION_A_START + i));
                        blockEntity.inventory.set(SLOT_POTION_A_START + i, ItemStack.EMPTY);
                    }
                }

                blockEntity.markDirty();
            } else if (!canCraft || !inputStack.isOf(blockEntity.itemBrewing)) {//Input item got switched to invalid one
                blockEntity.brewTime = 0;
                blockEntity.markDirty();
            }
        }
        else if (canCraft && blockEntity.fuel > 0) {
            --blockEntity.fuel;//Start new brew
            blockEntity.brewTime = blockEntity.brewTimeStart;
            blockEntity.itemBrewing = inputStack.getItem();
            blockEntity.markDirty();
        }

        //Block state for which bottles are shown
        boolean[] slotsEmpty = blockEntity.getPotionASlotsEmpty(blockEntity);
        if (!Arrays.equals(slotsEmpty, blockEntity.slotsEmptyLastTick)) {
            blockEntity.slotsEmptyLastTick = slotsEmpty;
            if (!(blockState.getBlock() instanceof BrewingStandBlock)) {
                return;
            }
            for (int i = 0; i < BrewingStandBlock.BOTTLE_PROPERTIES.length; ++i) {
                blockState = blockState.with(BrewingStandBlock.BOTTLE_PROPERTIES[i], slotsEmpty[i]);
            }
            world.setBlockState(blockPos, blockState, Block.NOTIFY_LISTENERS);
        }
    }

    public boolean[] getPotionASlotsEmpty(AutoBrewingStandBlockEntity blockEntity){
        boolean[] bls = new boolean[3];
        for (int i = SLOT_POTION_A_START; i <= SLOT_POTION_A_END; i++) {
            if (this.inventory.get(i).isEmpty()) continue;
            bls[i - SLOT_POTION_A_START] = true;
        }
        return bls;
    }

    private static boolean canCraft(DefaultedList<ItemStack> slots, World world) {
        ItemStack input = slots.get(SLOT_INPUT);
        if (input.isEmpty()) {
            return false;
        } else if (!world.getBrewingRecipeRegistry().isValidIngredient(input)) {
            return false;
        } else {
            for(int i = SLOT_POTION_A_START; i < SLOT_POTION_A_END; ++i) {
                ItemStack bottleStack = slots.get(i);
                if (!bottleStack.isEmpty() && world.getBrewingRecipeRegistry().hasRecipe(bottleStack, input)) {
                    return true;
                }
            }

            return false;
        }
    }

    private static void craft(World world, BlockPos pos, DefaultedList<ItemStack> slots) {
        ItemStack inputStack = slots.get(SLOT_INPUT);
        for (int i = SLOT_POTION_A_START; i <= SLOT_POTION_A_END; ++i) {
            slots.set(i, world.getBrewingRecipeRegistry().craft(inputStack, slots.get(i)));
        }
        inputStack.decrement(1);
        if (inputStack.getItem().hasRecipeRemainder()) {
            ItemStack newInputStack = new ItemStack(inputStack.getItem().getRecipeRemainder());
            if (inputStack.isEmpty()) {
                inputStack = newInputStack;
            } else {
                ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), newInputStack);
            }
        }
        slots.set(SLOT_INPUT, inputStack);
        //world.syncWorldEvent(WorldEvents.BREWING_STAND_BREWS, pos, 0);
    }

    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoBrewingStandScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(dir != Direction.UP && dir != Direction.DOWN){
            if(slot == SLOT_BLAZE_POWDER){
                return stack.isOf(Items.BLAZE_POWDER);
            }
            else if(slot >= SLOT_POTION_A_START && slot <= SLOT_POTION_A_END) {
                return (stack.isOf(Items.POTION) || stack.isOf(Items.SPLASH_POTION) || stack.isOf(Items.LINGERING_POTION) || stack.isOf(Items.GLASS_BOTTLE)) && this.getStack(slot).isEmpty();
            }
            return false;
        }

        if (slot == SLOT_INPUT) {
            return world.getBrewingRecipeRegistry().isValidIngredient(stack);
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot >= SLOT_POTION_B_START && slot <= SLOT_POTION_B_END;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : inventory) {
            if (!itemStack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup registryLookup) {
        return createNbt(registryLookup);
    }
}
