package uk.co.cablepost.autoworkstations.auto_brewing_stand;

import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilScreen;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.gold.GoldAutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.gold.GoldAutoBrewingStandBlockEntity;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.iron.IronAutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.iron.IronAutoBrewingStandBlockEntity;

public class AutoBrewingStandRegister {
    //screen
    public static final Identifier AUTO_BREWING_STAND_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_brewing_stand");
    public static final ScreenHandlerType<AutoBrewingStandScreenHandler> AUTO_BREWING_STAND_SCREEN_HANDLER = new ScreenHandlerType<>(AutoBrewingStandScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoBrewingStandBlock IRON_AUTO_BREWING_STAND_BLOCK = new IronAutoBrewingStandBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_BREWING_STAND_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_brewing_stand");
    public static BlockEntityType<IronAutoBrewingStandBlockEntity> IRON_AUTO_BREWING_STAND_BLOCK_ENTITY;

    //gold
    public static AutoBrewingStandBlock GOLD_AUTO_BREWING_STAND_BLOCK = new GoldAutoBrewingStandBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_BREWING_STAND_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_brewing_stand");
    public static BlockEntityType<GoldAutoBrewingStandBlockEntity> GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY;

    public static void onInitialize(){
        //iron
        IRON_AUTO_BREWING_STAND_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_BREWING_STAND_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoBrewingStandBlockEntity::new,
                        IRON_AUTO_BREWING_STAND_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_BREWING_STAND_IDENTIFIER, IRON_AUTO_BREWING_STAND_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_BREWING_STAND_IDENTIFIER, new BlockItem(IRON_AUTO_BREWING_STAND_BLOCK, new Item.Settings()));

        //gold
        GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_BREWING_STAND_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoBrewingStandBlockEntity::new,
                        GOLD_AUTO_BREWING_STAND_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_BREWING_STAND_IDENTIFIER, GOLD_AUTO_BREWING_STAND_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_BREWING_STAND_IDENTIFIER, new BlockItem(GOLD_AUTO_BREWING_STAND_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_BREWING_STAND_IDENTIFIER, AUTO_BREWING_STAND_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        BlockRenderLayerMap.INSTANCE.putBlock(IRON_AUTO_BREWING_STAND_BLOCK, RenderLayer.getTranslucent());
        BlockRenderLayerMap.INSTANCE.putBlock(GOLD_AUTO_BREWING_STAND_BLOCK, RenderLayer.getTranslucent());

        HandledScreens.register(AUTO_BREWING_STAND_SCREEN_HANDLER, AutoBrewingStandScreen::new);
    }
}
