package uk.co.cablepost.autoworkstations.auto_brewing_stand;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.BrewingStandScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilScreenHandler;


public class AutoBrewingStandScreen extends HandledScreen<AutoBrewingStandScreenHandler> {

    private static final Identifier TEXTURE = Identifier.of(AutoWorkstations.MOD_ID, "textures/gui/container/auto_brewing_stand.png");

    private static final int[] BUBBLE_PROGRESS = new int[]{29, 24, 20, 16, 11, 6, 0};

    public AutoBrewingStandScreen(AutoBrewingStandScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 198;
        this.titleY = 5;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        int k = handler.getFuel();
        int l = MathHelper.clamp((18 * k + 20 - 1) / 20, 0, 18);
        if (l > 0) {
            context.drawTexture(TEXTURE, x + 60, y + 44, 176, 29, l, 4);
        }

        int brewTime;
        if ((brewTime = handler.getBrewTime()) > 0) {
            int n = (int)(28.0f * (1.0f - (float)brewTime / handler.getStartBrewTime()));
            if (n > 0) {
                context.drawTexture(TEXTURE, x + 97, y + 16, 176, 0, 9, n);
            }
            if ((n = BUBBLE_PROGRESS[brewTime / 2 % 7]) > 0) {
                context.drawTexture(TEXTURE, x + 149, y + 56 + 29 - n, 185, 29 - n, 12, n);//big in bottom right
                context.drawTexture(TEXTURE, x + 64, y + 13 + 29 - n, 209, 18 - n, 12, n);//small in top left
            }
        }
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(context, mouseX, mouseY);
    }

    protected void drawForeground(DrawContext context, int mouseX, int mouseY) {
        context.drawText(this.textRenderer, this.title, this.titleX, this.titleY, 4210752, false);
    }
}
