package uk.co.cablepost.autoworkstations.auto_brewing_stand;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.BrewingStandScreenHandler;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;

public class AutoBrewingStandScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public AutoBrewingStandScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(AutoBrewingStandBlockEntity.INVENTORY_SIZE), new ArrayPropertyDelegate(3));
    }

    protected AutoBrewingStandScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(AutoBrewingStandRegister.AUTO_BREWING_STAND_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_INPUT, 79, 17));
        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_BLAZE_POWDER, 17, 17));

        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_A_START, 56, 58));
        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_A_START + 1, 79, 58));
        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_A_START + 2, 102, 58));

        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_B_START, 56, 58 + 31));
        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_B_START + 1, 79, 58 + 31));
        this.addSlot(new Slot(inventory, AutoBrewingStandBlockEntity.SLOT_POTION_B_START + 2, 102, 58 + 31));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 116 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 174));
        }
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack quickMove(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStackNoCallbacks(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    public int getFuel() {
        return this.propertyDelegate.get(1);
    }

    public int getBrewTime() {
        return this.propertyDelegate.get(0);
    }

    public int getStartBrewTime() {
        return this.propertyDelegate.get(2);
    }
}
