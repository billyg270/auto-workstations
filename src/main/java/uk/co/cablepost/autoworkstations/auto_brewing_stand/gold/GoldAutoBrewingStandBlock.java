package uk.co.cablepost.autoworkstations.auto_brewing_stand.gold;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BrewingStandBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class GoldAutoBrewingStandBlock extends AutoBrewingStandBlock {

    public GoldAutoBrewingStandBlock(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldAutoBrewingStandBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return
                world.isClient ?
                        GoldAutoBrewingStandBlock.validateTicker(type, AutoBrewingStandRegister.GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY, GoldAutoBrewingStandBlockEntity::clientTick) :
                        GoldAutoBrewingStandBlock.validateTicker(type, AutoBrewingStandRegister.GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY, GoldAutoBrewingStandBlockEntity::serverTick)
                ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.gold_auto_brewing_stand.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
