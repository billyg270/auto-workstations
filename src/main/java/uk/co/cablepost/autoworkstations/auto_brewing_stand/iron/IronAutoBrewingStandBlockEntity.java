package uk.co.cablepost.autoworkstations.auto_brewing_stand.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandBlockEntity;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandRegister;

public class IronAutoBrewingStandBlockEntity extends AutoBrewingStandBlockEntity {
    public IronAutoBrewingStandBlockEntity(BlockPos pos, BlockState state){
        super(AutoBrewingStandRegister.IRON_AUTO_BREWING_STAND_BLOCK_ENTITY, pos, state, 400);
    }
}
