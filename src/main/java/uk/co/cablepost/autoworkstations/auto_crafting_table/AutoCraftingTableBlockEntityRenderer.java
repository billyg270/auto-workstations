package uk.co.cablepost.autoworkstations.auto_crafting_table;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.RotationAxis;
import net.minecraft.world.World;
import org.joml.Vector3f;

import java.util.Objects;

public class AutoCraftingTableBlockEntityRenderer<T extends AutoCraftingTableBlockEntity> implements BlockEntityRenderer<T> {

    public AutoCraftingTableBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
    }

    @Override
    public void render(AutoCraftingTableBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        matrices.push();

        //ItemStack barItem = new ItemStack(Items.IRON_BLOCK);
        ItemStack barItem = new ItemStack(Items.LIGHT_GRAY_CONCRETE);

        //bar
        matrices.translate(blockEntity.animX, 0.96f, 0.5f);
        matrices.scale(0.05f, 0.05f, 0.95f);
        MinecraftClient.getInstance().getItemRenderer().renderItem(barItem, ModelTransformationMode.NONE, light, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
        matrices.scale(20f, 20f, 1f/0.95f);
        matrices.translate(-blockEntity.animX, -0.96f, -0.5f);

        //head
        matrices.translate(blockEntity.animX, 0.89f, blockEntity.animZ);
        matrices.scale(0.1f, 0.2f, 0.1f);
        MinecraftClient.getInstance().getItemRenderer().renderItem(barItem, ModelTransformationMode.NONE, light, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
        matrices.scale(10f, 5f, 10f);
        matrices.translate(-blockEntity.animX, -0.89f, -blockEntity.animZ);

        if(blockEntity.state != AutoCraftingTableBlockEntity.WORKING_STATE){
            matrices.pop();
            return;
        }

        float gridHeight = 0.62f;
        float holdingHeight = 0.66f;

        for(int i = 0; i < 9; i++){
            if(i > blockEntity.nextAnimSlot){
                continue;
            }

            ItemStack stack = blockEntity.getStack(i + AutoCraftingTableBlockEntity.PATTERN_SLOT_START);
            if(blockEntity.slotActuallyHasItem[i + AutoCraftingTableBlockEntity.PATTERN_SLOT_START] == 0 || stack.isEmpty()){
                continue;
            }

            if(i < blockEntity.nextAnimSlot || blockEntity.animToCorner){
                //item in grid
                float slotX = AutoCraftingTableBlockEntity.ANIM_SLOT_START_X + (i % 3) * AutoCraftingTableBlockEntity.ANIM_SLOT_GAP_X;
                float slotZ = AutoCraftingTableBlockEntity.ANIM_SLOT_START_Z + ((float) Math.floor(i / 3f) * AutoCraftingTableBlockEntity.ANIM_SLOT_GAP_Z);
                renderItem(matrices, vertexConsumers, light, overlay, stack, new Vector3f(slotX, gridHeight, slotZ), blockEntity.getWorld());
            }
            else{
                //holding something
                renderItem(matrices, vertexConsumers, light, overlay, stack, new Vector3f(blockEntity.animX, holdingHeight, blockEntity.animZ), blockEntity.getWorld());
            }
        }

        matrices.pop();
    }

    public void renderItem(MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay, ItemStack itemStack, Vector3f pos, World world){
        boolean hasDepth = Objects.requireNonNull(MinecraftClient.getInstance().getItemRenderer().getModels().getModel(itemStack.getItem())).hasDepth();

        if(!hasDepth){
            pos.add(0f, 0.08f, 0.05f);
        }

        float itemScale = 0.4f;

        matrices.translate(pos.x(), pos.y(), pos.z());

        matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(180));

        if(!hasDepth){
            matrices.multiply(RotationAxis.POSITIVE_X.rotationDegrees(90));
        }

        matrices.scale(itemScale, itemScale, itemScale);

        MinecraftClient.getInstance().getItemRenderer().renderItem(itemStack, ModelTransformationMode.GROUND, light, overlay, matrices, vertexConsumers, world, 0);

        matrices.scale(1/itemScale, 1/itemScale, 1/itemScale);

        if(!hasDepth){
            matrices.multiply(RotationAxis.POSITIVE_X.rotationDegrees(-90));
        }

        matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(-180));

        matrices.translate(-pos.x(), -pos.y(), -pos.z());
    }
}
