package uk.co.cablepost.autoworkstations.auto_crafting_table;

import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.gold.GoldAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.gold.GoldAutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.iron.IronAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.iron.IronAutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlockEntity;

public class AutoCraftingTableRegister {
    //screen
    public static final Identifier AUTO_CRAFTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_crafting_table");
    public static final ScreenHandlerType<AutoCraftingTableScreenHandler> AUTO_CRAFTING_TABLE_SCREEN_HANDLER = new ScreenHandlerType<>(AutoCraftingTableScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoCraftingTableBlock IRON_AUTO_CRAFTING_TABLE_BLOCK = new IronAutoCraftingTableBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_CRAFTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_crafting_table");
    public static BlockEntityType<IronAutoCraftingTableBlockEntity> IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    //gold
    public static AutoCraftingTableBlock GOLD_AUTO_CRAFTING_TABLE_BLOCK = new GoldAutoCraftingTableBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_crafting_table");
    public static BlockEntityType<GoldAutoCraftingTableBlockEntity> GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    //netherite
    public static AutoCraftingTableBlock NETHERITE_AUTO_CRAFTING_TABLE_BLOCK = new NetheriteAutoCraftingTableBlock(FabricBlockSettings.create().mapColor(MapColor.DARK_DULL_PINK).sounds(BlockSoundGroup.METAL).strength(3.0f));
    public static final Identifier NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "netherite_auto_crafting_table");
    public static BlockEntityType<NetheriteAutoCraftingTableBlockEntity> NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    public static void onInitialize(){
        //iron

        IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoCraftingTableBlockEntity::new,
                        IRON_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_CRAFTING_TABLE_IDENTIFIER, IRON_AUTO_CRAFTING_TABLE_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(IRON_AUTO_CRAFTING_TABLE_BLOCK, new Item.Settings()));

        //gold

        GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoCraftingTableBlockEntity::new,
                        GOLD_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER, GOLD_AUTO_CRAFTING_TABLE_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(GOLD_AUTO_CRAFTING_TABLE_BLOCK, new Item.Settings()));

        //netherite

        NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteAutoCraftingTableBlockEntity::new,
                        NETHERITE_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER, NETHERITE_AUTO_CRAFTING_TABLE_BLOCK);
        Registry.register(Registries.ITEM, NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(NETHERITE_AUTO_CRAFTING_TABLE_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_CRAFTING_TABLE_IDENTIFIER, AUTO_CRAFTING_TABLE_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        BlockEntityRendererRegistry.register(IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);

        HandledScreens.register(AUTO_CRAFTING_TABLE_SCREEN_HANDLER, AutoCraftingTableScreen::new);
    }
}
