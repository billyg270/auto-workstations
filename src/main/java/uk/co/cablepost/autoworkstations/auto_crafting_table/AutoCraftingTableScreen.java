package uk.co.cablepost.autoworkstations.auto_crafting_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoCraftingTableScreen extends HandledScreen<AutoCraftingTableScreenHandler> {
    //A path to the gui texture. In this example we use the texture from the dispenser
    private static final Identifier TEXTURE = Identifier.of(AutoWorkstations.MOD_ID, "textures/gui/container/auto_crafting_table.png");

    public AutoCraftingTableScreen(AutoCraftingTableScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundHeight = 245;
        this.playerInventoryTitleY = 152;
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.recipe_pattern_1"), x + 10, y + 25, 0x404040, false);
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.recipe_pattern_2"), x + 10, y + 35, 0x404040, false);
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.input"), x + 8, y + 83, 0x404040, false);
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.output"), x + 8, y + 114, 0x404040, false);

        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status"), x + 124, y + 32, 0x404040, false);
        int state = handler.getState();
        if(state == AutoCraftingTableBlockEntity.WORKING_STATE){
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.crafting"), x + 124, y + 46, 0x4eba3c, false);
        }
        else if(state == AutoCraftingTableBlockEntity.NO_RECIPE_STATE){
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.invalid_recipe_1"), x + 124, y + 46, 0xd62d2d, false);
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.invalid_recipe_2"), x + 124, y + 54, 0xd62d2d, false);
        }
        else if(state == AutoCraftingTableBlockEntity.RECIPE_LOCKED_STATE){
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.unknown_recipe_1"), x + 124, y + 46, 0xd62020, false);
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.unknown_recipe_2"), x + 124, y + 54, 0xd62020, false);
        }
        else if(state == AutoCraftingTableBlockEntity.OUTPUT_FULL_STATE){
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.output_full_1"), x + 124, y + 46, 0xd47c1e, false);
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.output_full_2"), x + 124, y + 54, 0xd47c1e, false);
        }
        else if(state == AutoCraftingTableBlockEntity.NOT_ENOUGH_RESOURCES_STATE){
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.lacking_materials_1"), x + 124, y + 46, 0xd47c1e, false);
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.lacking_materials_2"), x + 124, y + 54, 0xd47c1e, false);
        }
        else{
            context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_crafting_table.status.offline"), x + 124, y + 46, 0xd62d2d, false);
        }
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        renderBackground(context, mouseX, mouseY, delta);
        super.render(context, mouseX, mouseY, delta);
        drawMouseoverTooltip(context, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}