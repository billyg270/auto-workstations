package uk.co.cablepost.autoworkstations.auto_crafting_table.gold;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class GoldAutoCraftingTableBlock extends AutoCraftingTableBlock {
    public static final MapCodec<GoldAutoCraftingTableBlock> CODEC = createCodec(GoldAutoCraftingTableBlock::new);

    public GoldAutoCraftingTableBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldAutoCraftingTableBlockEntity(pos, state);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                validateTicker(type, AutoCraftingTableRegister.GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, GoldAutoCraftingTableBlockEntity::clientTick) :
                validateTicker(type, AutoCraftingTableRegister.GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, GoldAutoCraftingTableBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.gold_auto_crafting_table.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
