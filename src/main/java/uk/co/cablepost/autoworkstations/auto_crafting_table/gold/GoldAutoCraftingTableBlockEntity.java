package uk.co.cablepost.autoworkstations.auto_crafting_table.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;

public class GoldAutoCraftingTableBlockEntity extends AutoCraftingTableBlockEntity {
    public GoldAutoCraftingTableBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoCraftingTableRegister.GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, blockPos, blockState);
        this.ANIM_SPEED = 0.1f;
    }
}
