package uk.co.cablepost.autoworkstations.auto_crafting_table.netherite;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;

public class NetheriteAutoCraftingTableBlockEntity extends AutoCraftingTableBlockEntity {
    public NetheriteAutoCraftingTableBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoCraftingTableRegister.NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, blockPos, blockState);
        this.ANIM_SPEED = 0.2f;
        this.BATCH_CRAFT_COUNT = 64;
    }
}
