package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import net.minecraft.block.BlockState;
import net.minecraft.block.EnchantingTableBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.registry.entry.RegistryEntryList;
import net.minecraft.registry.tag.EnchantmentTags;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import net.minecraft.util.math.random.LocalRandom;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoEnchantingTableBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {
    public static int INVENTORY_SIZE = 5;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static int SLOT_EXP_BOTTLE = 0;
    public static int SLOT_EMPTY_BOTTLE = 1;
    public static int SLOT_ITEM_TO_ENCHANT = 2;
    public static int SLOT_LAPIS = 3;
    public static int SLOT_ENCHANTED_ITEM = 4;

    public int enchantSeed;

    public LocalRandom random;

    int xpEmptyBottleProgress;
    public int xpEmptyBottleMaxProgress = 20;
    int expLevel;
    float expProgress;

    public int[] enchantPowers = new int[3];
    //public int[] enchantLevels = new int[3];

    public int selectedEnchantTier = 3;//1, 2 or 3

    private boolean canDoEnchantPowers = false;
    private boolean canDoEnchantRequirements = false;
    public ItemStack lastToEnchantItemStack = null;

    public int enchantProgress;
    public int maxEnchantProgress = 60;

    public int bookshelfCount = -1;

    public float targetBookRotation;
    public float bookRotation;
    public float lastBookRotation;

    public AutoEnchantingTableBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        enchantSeed = generateEnchantSeed();

        random = new LocalRandom(enchantSeed);

        xpEmptyBottleMaxProgress = 20;
        maxEnchantProgress = 60;
    }

    @Override
    public void readNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(nbt, wrapperLookup);

        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory, wrapperLookup);

        enchantSeed = nbt.getInt("EnchantSeed");

        xpEmptyBottleProgress = nbt.getInt("XpEmptyBottleProgress");
        xpEmptyBottleMaxProgress = nbt.getInt("XpEmptyBottleMaxProgress");
        if(xpEmptyBottleMaxProgress == 0){
            xpEmptyBottleMaxProgress = 20;
        }

        expLevel = nbt.getInt("ExpLevel");
        expProgress = nbt.getFloat("ExpProgress");

        enchantPowers = nbt.getIntArray("EnchantPowers");
        //enchantLevels = nbt.getIntArray("EnchantLevels");

        selectedEnchantTier = nbt.getInt("SelectedEnchantTier");

        canDoEnchantPowers = nbt.getBoolean("CanDoEnchantPowers");
        canDoEnchantRequirements = nbt.getBoolean("CanDoEnchantRequirements");

        enchantProgress = nbt.getInt("EnchantProgress");
        maxEnchantProgress = nbt.getInt("MaxEnchantProgress");
        if(maxEnchantProgress == 0){
            maxEnchantProgress = 60;
        }
    }

    @Override
    protected void writeNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.writeNbt(nbt, wrapperLookup);

        Inventories.writeNbt(nbt, inventory, wrapperLookup);

        nbt.putInt("EnchantSeed", enchantSeed);

        nbt.putInt("XpEmptyBottleProgress", xpEmptyBottleProgress);
        nbt.putInt("XpEmptyBottleMaxProgress", xpEmptyBottleMaxProgress);
        nbt.putInt("ExpLevel", expLevel);
        nbt.putFloat("ExpProgress", expProgress);

        nbt.putIntArray("EnchantPowers", enchantPowers);
        //nbt.putIntArray("EnchantLevels", enchantLevels);

        nbt.putInt("SelectedEnchantTier", selectedEnchantTier);

        nbt.putBoolean("CanDoEnchantPowers", canDoEnchantPowers);
        nbt.putBoolean("CanDoEnchantRequirements", canDoEnchantRequirements);

        nbt.putInt("EnchantProgress", enchantProgress);
        nbt.putInt("MaxEnchantProgress", maxEnchantProgress);
    }

    public static int PROPERTY_DELEGATE_SIZE = 7;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0: {
                    return Math.round(AutoEnchantingTableBlockEntity.this.expProgress * 100f);
                }
                case 1: {
                    return AutoEnchantingTableBlockEntity.this.expLevel;
                }
                case 2: {
                    return xpEmptyBottleProgress;
                }
                case 3: {
                    return xpEmptyBottleMaxProgress;
                }
                case 4: {
                    return AutoEnchantingTableBlockEntity.this.enchantProgress;
                }
                case 5: {
                    return AutoEnchantingTableBlockEntity.this.maxEnchantProgress;
                }
                case 6: {
                    return AutoEnchantingTableBlockEntity.this.selectedEnchantTier;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public static int generateEnchantSeed(){
        return new Random().nextInt();
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoEnchantingTableBlockEntity blockEntity) {
        //----------------------------------------------------------------
        float g;
        blockEntity.lastBookRotation = blockEntity.bookRotation;
        PlayerEntity playerEntity = world.getClosestPlayer((double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5, 3.0, false);
        if (playerEntity != null) {
            double d = playerEntity.getX() - ((double)pos.getX() + 0.5);
            double e = playerEntity.getZ() - ((double)pos.getZ() + 0.5);
            blockEntity.targetBookRotation = (float) MathHelper.atan2(e, d);
        } else {
            blockEntity.targetBookRotation += 0.02f;
        }
        while (blockEntity.bookRotation >= (float)Math.PI) {
            blockEntity.bookRotation -= (float)Math.PI * 2;
        }
        while (blockEntity.bookRotation < (float)(-Math.PI)) {
            blockEntity.bookRotation += (float)Math.PI * 2;
        }
        while (blockEntity.targetBookRotation >= (float)Math.PI) {
            blockEntity.targetBookRotation -= (float)Math.PI * 2;
        }
        while (blockEntity.targetBookRotation < (float)(-Math.PI)) {
            blockEntity.targetBookRotation += (float)Math.PI * 2;
        }
        for (g = blockEntity.targetBookRotation - blockEntity.bookRotation; g >= (float)Math.PI; g -= (float)Math.PI * 2) {
        }
        while (g < (float)(-Math.PI)) {
            g += (float)Math.PI * 2;
        }
        blockEntity.bookRotation += g * 0.4f;

        //----------------------------------------------------------------

    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoEnchantingTableBlockEntity blockEntity) {
        boolean toMarkDirty = false;

        //empty xp bottles

        ItemStack expBottlesStack = blockEntity.inventory.get(SLOT_EXP_BOTTLE);
        ItemStack emptyBottlesStack = blockEntity.inventory.get(SLOT_EMPTY_BOTTLE);

        if(
                (
                        //An exp bottle to use
                        !expBottlesStack.isEmpty() &&
                                expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE)
                )
                        &&
                        (
                                //space in output
                                emptyBottlesStack.isEmpty() ||
                                        (
                                                emptyBottlesStack.isOf(Items.GLASS_BOTTLE) &&
                                                        emptyBottlesStack.getCount() < emptyBottlesStack.getMaxCount()
                                        )
                        )
        ){
            blockEntity.xpEmptyBottleProgress += 1;

            if(blockEntity.xpEmptyBottleProgress >= blockEntity.xpEmptyBottleMaxProgress) {
                blockEntity.addExperience(Math.round(AutoWorkstations.EXPERIENCE_BOTTLE_VALUE));
                expBottlesStack.decrement(1);
                if (emptyBottlesStack.isEmpty()) {
                    emptyBottlesStack = new ItemStack(Items.GLASS_BOTTLE);
                } else {
                    emptyBottlesStack.increment(1);
                }

                blockEntity.inventory.set(SLOT_EMPTY_BOTTLE, emptyBottlesStack);
                blockEntity.xpEmptyBottleProgress = 0;

                toMarkDirty = true;
            }
        }
        else{
            blockEntity.xpEmptyBottleProgress = 0;
        }

        //enchant stuff

        ItemStack toEnchant = blockEntity.getStack(SLOT_ITEM_TO_ENCHANT);

        boolean bookshelfCountChanged = blockEntity.updateBookshelfCount();

        if(
                blockEntity.lastToEnchantItemStack == null ||
                        blockEntity.lastToEnchantItemStack.isEmpty() != toEnchant.isEmpty() ||
                        blockEntity.lastToEnchantItemStack.getItem() != toEnchant.getItem() ||
                        blockEntity.lastToEnchantItemStack.getCount() != toEnchant.getCount() ||
                        bookshelfCountChanged
        ){
            blockEntity.lastToEnchantItemStack = blockEntity.getStack(SLOT_ITEM_TO_ENCHANT).copy();
            ItemStack singularItemToEnchant = toEnchant.copy();
            singularItemToEnchant.setCount(1);
            if(!toEnchant.isEmpty() && singularItemToEnchant.isEnchantable()){
                for(int i = 0; i < 10; i++) {
                    blockEntity.canDoEnchantPowers = blockEntity.updateEnchantPowers();
                    if(blockEntity.enchantPowers[blockEntity.selectedEnchantTier - 1] > 0){
                        break;
                    }
                }
            }
            else{
                blockEntity.canDoEnchantPowers = false;
            }
        }

        if(world.getTime() % 20 == 0){
            if(!toEnchant.isEmpty()){
                ItemStack toEnchantSingle = toEnchant.copy();
                toEnchantSingle.setCount(1);
                if(!toEnchantSingle.isEnchantable()) {
                    world.playSound(
                            null,
                            pos,
                            SoundEvents.BLOCK_NOTE_BLOCK_DIDGERIDOO.value(),
                            SoundCategory.BLOCKS,
                            0.5f,
                            1f
                    );
                }
            }
        }

        if(blockEntity.canDoEnchantPowers) {
            blockEntity.canDoEnchantRequirements = blockEntity.canDoEnchant();
        }
        else{
            blockEntity.canDoEnchantRequirements = false;
        }

        if(blockEntity.canDoEnchantPowers && blockEntity.canDoEnchantRequirements){
            blockEntity.enchantProgress ++;
            if(blockEntity.enchantProgress >= blockEntity.maxEnchantProgress) {
                blockEntity.doEnchant();
                blockEntity.enchantProgress = 0;
                blockEntity.lastToEnchantItemStack = null;
            }
        }

        if(!blockEntity.canDoEnchantPowers || !blockEntity.canDoEnchantRequirements){
            blockEntity.enchantProgress = 0;
        }

        if (toMarkDirty) {
            blockEntity.markDirty();
        }
    }

    public int getBookshelfCount(){
        int i = 0;
        for (BlockPos blockPos : EnchantingTableBlock.POWER_PROVIDER_OFFSETS) {
            if (!EnchantingTableBlock.canAccessPowerProvider(world, pos, blockPos)) continue;
            ++i;
        }
        return i;
    }

    public boolean updateBookshelfCount(){
        int newBookshelfCount = getBookshelfCount();
        if(bookshelfCount != newBookshelfCount){
            bookshelfCount = newBookshelfCount;
            return true;
        }
        return false;
    }

    public boolean updateEnchantPowers(){
        //enchantPowers
        ItemStack toEnchant = getStack(SLOT_ITEM_TO_ENCHANT);
        ItemStack singularItemToEnchant = toEnchant.copy();
        singularItemToEnchant.setCount(1);

        if(toEnchant.isEmpty() || !singularItemToEnchant.isEnchantable()){
            enchantPowers = new int[]{ 0,  0,  0};
            //enchantLevels = new int[]{-1, -1, -1};

            return false;
        }

        for (int i = 0; i < 3; ++i) {
            //enchantmentIds[i] = -1;
            //enchantLevels[i] = -1;

            enchantPowers[i] = EnchantmentHelper.calculateRequiredExperienceLevel(random, i, bookshelfCount, singularItemToEnchant);
            if (enchantPowers[i] < i + 1) {
                enchantPowers[i] = 0;
            }

            /*
            if(enchantPowers[i] <= 0){
                continue;
            }

            List<EnchantmentLevelEntry> enchants = this.generateEnchantments(toEnchant, enchantPowers[i]);

            if(enchants == null || enchants.isEmpty()){
                continue;
            }

            EnchantmentLevelEntry enchantmentLevelEntry = enchants.get(random.nextInt(enchants.size()));
            */

            //enchantmentIds[j] = Registry.ENCHANTMENT.getRawId(enchantmentLevelEntry.enchantment);
            //enchantLevels[i] = enchantmentLevelEntry.level;
        }

        if(enchantPowers[selectedEnchantTier - 1] <= 0){
            return false;
        }

        return true;
    }

    public boolean canDoEnchant(){
        if(expLevel < enchantPowers[selectedEnchantTier - 1]){
            return false;//not enough levels
        }

        ItemStack lapis = getStack(SLOT_LAPIS);

        if(lapis.isEmpty() || lapis.getCount() < selectedEnchantTier){
            return false;//not enough lapis
        }

        ItemStack outputSlot = getStack(SLOT_ENCHANTED_ITEM);
        if(!outputSlot.isEmpty()){
            return false;
        }

        return true;
    }

    public void doEnchant(){
        ItemStack lapis = getStack(SLOT_LAPIS);
        ItemStack itemEnchanting = getStack(SLOT_ITEM_TO_ENCHANT).copy();
        itemEnchanting.setCount(1);

        getStack(SLOT_ITEM_TO_ENCHANT).decrement(1);
        if (getStack(SLOT_ITEM_TO_ENCHANT).isEmpty()) {
            setStack(SLOT_ITEM_TO_ENCHANT, ItemStack.EMPTY);
        }

        List<EnchantmentLevelEntry> enchants = this.generateEnchantments(world.getEnabledFeatures(), itemEnchanting, enchantPowers[selectedEnchantTier - 1]);

        if (enchants != null && !enchants.isEmpty()) {
            addExperienceLevels(-selectedEnchantTier);

            lapis.decrement(selectedEnchantTier);
            if (lapis.isEmpty()) {
                setStack(SLOT_LAPIS, ItemStack.EMPTY);
            }

            boolean isBook = itemEnchanting.isOf(Items.BOOK);

            if (isBook) {
                itemEnchanting = itemEnchanting.copyComponentsToNewStack(Items.ENCHANTED_BOOK, 1);
            }

            for (EnchantmentLevelEntry enchantmentLevelEntry : enchants) {
                itemEnchanting.addEnchantment(enchantmentLevelEntry.enchantment, enchantmentLevelEntry.level);
            }

            world.playSound(null, pos, SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
        }
        else{
            world.playSound(null, pos, SoundEvents.BLOCK_NOTE_BLOCK_DIDGERIDOO.value(), SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
        }

        setStack(SLOT_ENCHANTED_ITEM, itemEnchanting);
    }

    private List<EnchantmentLevelEntry> generateEnchantments(FeatureSet enabledFeatures, ItemStack stack, int level) {
        //random.setSeed(enchantSeed + slot);
        Optional<RegistryEntryList.Named<Enchantment>> optional = world.getRegistryManager().get(RegistryKeys.ENCHANTMENT).getEntryList(EnchantmentTags.IN_ENCHANTING_TABLE);
        List<EnchantmentLevelEntry> list = EnchantmentHelper.generateEnchantments(this.random, stack, level, optional.get().stream());
        if (stack.isOf(Items.BOOK) && list.size() > 1) {
            list.remove(random.nextInt(list.size()));
        }
        return list;
    }

    public void addExperience(int experience) {
        this.expProgress += (float)experience / (float)this.getNextLevelExperience();
        while (this.expProgress < 0.0f) {
            float f = this.expProgress * (float)this.getNextLevelExperience();
            if (this.expLevel > 0) {
                this.addExperienceLevels(-1);
                this.expProgress = 1.0f + f / (float)this.getNextLevelExperience();
                continue;
            }
            this.addExperienceLevels(-1);
            this.expProgress = 0.0f;
        }
        while (this.expProgress >= 1.0f) {
            this.expProgress = (this.expProgress - 1.0f) * (float)this.getNextLevelExperience();
            this.addExperienceLevels(1);
            this.expProgress /= (float)this.getNextLevelExperience();
        }
    }

    public void addExperienceLevels(int levels) {
        this.expLevel += levels;
        if (this.expLevel < 0) {
            this.expLevel = 0;
            this.expProgress = 0.0f;
        }
    }

    public int getNextLevelExperience() {
        if (this.expLevel >= 30) {
            return 112 + (this.expLevel - 30) * 9;
        }
        if (this.expLevel >= 15) {
            return 37 + (this.expLevel - 15) * 5;
        }
        return 7 + this.expLevel * 2;
    }

    /**
     * Returns the title of this screen handler; will be a part of the open
     * screen packet sent to the client.
     */
    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoEnchantingTableScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(dir == Direction.UP || dir == Direction.DOWN){
            return slot == SLOT_ITEM_TO_ENCHANT;
        }

        if (slot == SLOT_EXP_BOTTLE) {
            return stack.isOf(Items.EXPERIENCE_BOTTLE);
        }

        if (slot == SLOT_LAPIS) {
            return stack.isOf(Items.LAPIS_LAZULI);
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == SLOT_EMPTY_BOTTLE || slot == SLOT_ENCHANTED_ITEM;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < inventory.size(); i++){
            if(!inventory.get(i).isEmpty()){
                return false;
            }
        }

        return true;
    }

    /**
     * Fetches the stack currently stored at the given slot. If the slot is empty,
     * or is outside the bounds of this inventory, returns see {@link ItemStack#EMPTY}.
     *
     * @param slot
     */
    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    /**
     * Removes a specific number of items from the given slot.
     *
     * @param slot
     * @param amount
     * @return the removed items as a stack
     */
    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    /**
     * Removes the stack currently stored at the indicated slot.
     *
     * @param slot
     * @return the stack previously stored at the indicated slot.
     */
    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup wrapperLookup) {
        return createNbt(wrapperLookup);
    }

    public void dropXP(){
        int amt = this.expLevel * 7;
        amt = Math.min(amt, 100);
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), amt);
    }
}
