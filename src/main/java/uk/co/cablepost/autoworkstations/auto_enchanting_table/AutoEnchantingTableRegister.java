package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.gold.GoldAutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.gold.GoldAutoEnchantingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.iron.IronAutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.iron.IronAutoEnchantingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceScreen;

public class AutoEnchantingTableRegister {
    //screen
    public static final Identifier AUTO_ENCHANTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_enchanting_table");
    public static final ScreenHandlerType<AutoEnchantingTableScreenHandler> AUTO_ENCHANTING_TABLE_SCREEN_HANDLER = new ScreenHandlerType<>(AutoEnchantingTableScreenHandler::new, FeatureSet.empty());

    //xp inside
    public static final Identifier AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_enchanting_table_xp_inside");
    public static AutoEnchantingTableXpInsideBlock AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK = new AutoEnchantingTableXpInsideBlock(FabricBlockSettings.create().strength(999f).nonOpaque().luminance(10));

    //iron
    public static AutoEnchantingTableBlock IRON_AUTO_ENCHANTING_TABLE_BLOCK = new IronAutoEnchantingTableBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.5f).nonOpaque().luminance(8));
    public static final Identifier IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_enchanting_table");
    public static BlockEntityType<IronAutoEnchantingTableBlockEntity> IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY;

    //gold
    public static AutoEnchantingTableBlock GOLD_AUTO_ENCHANTING_TABLE_BLOCK = new GoldAutoEnchantingTableBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.5f).nonOpaque().luminance(8));
    public static final Identifier GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_enchanting_table");
    public static BlockEntityType<GoldAutoEnchantingTableBlockEntity> GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY;

    public static void onInitialize(){
        //iron
        IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoEnchantingTableBlockEntity::new,
                        IRON_AUTO_ENCHANTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER, IRON_AUTO_ENCHANTING_TABLE_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER, new BlockItem(IRON_AUTO_ENCHANTING_TABLE_BLOCK, new Item.Settings()));

        //gold
        GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoEnchantingTableBlockEntity::new,
                        GOLD_AUTO_ENCHANTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER, GOLD_AUTO_ENCHANTING_TABLE_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER, new BlockItem(GOLD_AUTO_ENCHANTING_TABLE_BLOCK, new Item.Settings()));

        //xp inside
        Registry.register(Registries.BLOCK, AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER, AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK);
        Registry.register(Registries.ITEM, AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER, new BlockItem(AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_ENCHANTING_TABLE_IDENTIFIER, AUTO_ENCHANTING_TABLE_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        BlockRenderLayerMap.INSTANCE.putBlock(IRON_AUTO_ENCHANTING_TABLE_BLOCK, RenderLayer.getTranslucent());
        BlockRenderLayerMap.INSTANCE.putBlock(GOLD_AUTO_ENCHANTING_TABLE_BLOCK, RenderLayer.getTranslucent());

        BlockEntityRendererRegistry.register(IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, AutoEnchantingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, AutoEnchantingTableBlockEntityRenderer::new);

        BlockRenderLayerMap.INSTANCE.putBlock(AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK, RenderLayer.getTranslucent());

        HandledScreens.register(AUTO_ENCHANTING_TABLE_SCREEN_HANDLER, AutoEnchantingTableScreen::new);
    }
}
