package uk.co.cablepost.autoworkstations.auto_enchanting_table.gold;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class GoldAutoEnchantingTableBlock extends AutoEnchantingTableBlock {
    public static final MapCodec<GoldAutoEnchantingTableBlock> CODEC = createCodec(GoldAutoEnchantingTableBlock::new);

    public GoldAutoEnchantingTableBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldAutoEnchantingTableBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                GoldAutoEnchantingTableBlock.validateTicker(type, AutoEnchantingTableRegister.GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, GoldAutoEnchantingTableBlockEntity::clientTick) :
                GoldAutoEnchantingTableBlock.validateTicker(type, AutoEnchantingTableRegister.GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, GoldAutoEnchantingTableBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.gold_auto_enchanting_table.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
