package uk.co.cablepost.autoworkstations.auto_enchanting_table.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoEnchantingTableBlock extends AutoEnchantingTableBlock {
    public static final MapCodec<IronAutoEnchantingTableBlock> CODEC = createCodec(IronAutoEnchantingTableBlock::new);

    public IronAutoEnchantingTableBlock(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoEnchantingTableBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                IronAutoEnchantingTableBlock.validateTicker(type, AutoEnchantingTableRegister.IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, IronAutoEnchantingTableBlockEntity::clientTick) :
                IronAutoEnchantingTableBlock.validateTicker(type, AutoEnchantingTableRegister.IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, IronAutoEnchantingTableBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_enchanting_table.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}

