package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.gold.GoldAutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.gold.GoldAutoExperienceOrbEmitterBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron.IronAutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron.IronAutoExperienceOrbEmitterBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumScreen;

public class AutoExperienceOrbEmitterRegister {
    public static final Identifier AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_experience_orb_emitter");
    public static final ScreenHandlerType<AutoExperienceOrbEmitterScreenHandler> AUTO_EXPERIENCE_ORB_EMITTER_SCREEN_HANDLER = new ScreenHandlerType<>(AutoExperienceOrbEmitterScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoExperienceOrbEmitterBlock IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK = new IronAutoExperienceOrbEmitterBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_experience_orb_emitter");
    public static BlockEntityType<IronAutoExperienceOrbEmitterBlockEntity> IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY;

    //gold
    public static AutoExperienceOrbEmitterBlock GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK = new GoldAutoExperienceOrbEmitterBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_experience_orb_emitter");
    public static BlockEntityType<GoldAutoExperienceOrbEmitterBlockEntity> GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY;

    public static void onInitialize(){
        //iron

        IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoExperienceOrbEmitterBlockEntity::new,
                        IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, new BlockItem(IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK, new Item.Settings()));

        //gold

        GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoExperienceOrbEmitterBlockEntity::new,
                        GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, new BlockItem(GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, AUTO_EXPERIENCE_ORB_EMITTER_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        HandledScreens.register(AUTO_EXPERIENCE_ORB_EMITTER_SCREEN_HANDLER, AutoExperienceOrbEmitterScreen::new);
    }
}
