package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter;

import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoExperienceOrbEmitterScreen extends HandledScreen<AutoExperienceOrbEmitterScreenHandler> {
    private static final Identifier TEXTURE = Identifier.of(AutoWorkstations.MOD_ID, "textures/gui/container/auto_experience_orb_emitter.png");

    public AutoExperienceOrbEmitterScreen(AutoExperienceOrbEmitterScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        int xpEmptyProgress = handler.getXpEmptyProgress();
        context.drawTexture(TEXTURE, x + 89, y + 41, 176, 55, 18, xpEmptyProgress);

        float bottlesCanFill = handler.getXpBottlesCouldFill();
        context.drawText(this.textRenderer, Text.of("x" + bottlesCanFill), x + 82, y + 50, 0x404040, false);
        context.drawText(this.textRenderer, Text.of("x" + bottlesCanFill), x + 81, y + 49, 0x33de00, false);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        renderBackground(context, mouseX, mouseY, delta);
        super.render(context, mouseX, mouseY, delta);
        drawMouseoverTooltip(context, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}
