package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.iron.IronAutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoExperienceOrbEmitterBlock extends AutoExperienceOrbEmitterBlock {
    public static final MapCodec<IronAutoExperienceOrbEmitterBlock> CODEC = createCodec(IronAutoExperienceOrbEmitterBlock::new);
    public IronAutoExperienceOrbEmitterBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoExperienceOrbEmitterBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                IronAutoExperienceOrbEmitterBlock.validateTicker(type, AutoExperienceOrbEmitterRegister.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, IronAutoExperienceOrbEmitterBlockEntity::clientTick) :
                IronAutoExperienceOrbEmitterBlock.validateTicker(type, AutoExperienceOrbEmitterRegister.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, IronAutoExperienceOrbEmitterBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_experience_orb_emitter.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.redstone_to_enable.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_RED) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
