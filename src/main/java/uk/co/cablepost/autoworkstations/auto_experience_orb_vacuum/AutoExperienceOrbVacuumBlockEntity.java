package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.TypeFilter;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlockEntity;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoExperienceOrbVacuumBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {
    public static int INVENTORY_SIZE = 2;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static int SLOT_EMPTY_BOTTLE = 1;
    public static int SLOT_EXP_BOTTLE = 0;

    int xpFillBottleProgress;
    public int xpFillBottleMaxProgress;
    int expTank;

    public float suckRange;

    List<ExperienceOrbEntity> xpOrbsNearList;

    public AutoExperienceOrbVacuumBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        xpFillBottleMaxProgress = 20;
        suckRange = 5f;
    }

    @Override
    public void readNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(nbt, wrapperLookup);
        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory, wrapperLookup);
        xpFillBottleProgress = nbt.getInt("XpFillBottleProgress");
        xpFillBottleMaxProgress = nbt.getInt("XpFillBottleMaxProgress");
        if(xpFillBottleMaxProgress == 0){
            xpFillBottleMaxProgress = 20;
        }
        expTank = nbt.getInt("ExpTank");
        suckRange = nbt.getFloat("SuckRange");
    }

    @Override
    protected void writeNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        nbt.putInt("XpFillBottleProgress", xpFillBottleProgress);
        nbt.putInt("XpFillBottleMaxProgress", xpFillBottleMaxProgress);
        nbt.putInt("ExpTank", expTank);
        nbt.putFloat("SuckRange", suckRange);
        Inventories.writeNbt(nbt, inventory, wrapperLookup);

        super.writeNbt(nbt, wrapperLookup);
    }

    public static int PROPERTY_DELEGATE_SIZE = 3;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0: {
                    return xpFillBottleProgress;
                }
                case 1: {
                    return xpFillBottleMaxProgress;
                }
                case 2: {
                    return expTank;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoExperienceOrbVacuumBlockEntity blockEntity) {
        blockEntity.commonTick();
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoExperienceOrbVacuumBlockEntity blockEntity) {
        blockEntity.commonTick();

        Boolean toMarkDirty = false;

        if(world == null){
            return;
        }

        if(blockEntity.xpOrbsNearList != null){
            for(ExperienceOrbEntity orb : blockEntity.xpOrbsNearList){
                Vec3d targetPos = new Vec3d(
                        pos.getX() + 0.5f,
                        pos.getY() + 1f,
                        pos.getZ() + 0.5f
                );
                Vec3d targetDirection = new Vec3d(
                        targetPos.x - orb.getX(),
                        targetPos.y - orb.getY(),
                        targetPos.z - orb.getZ()
                );
                double dis = targetDirection.lengthSquared();

                if(dis < 0.1f){
                    if(orb.getRemovalReason() == null){
                        blockEntity.expTank += orb.getExperienceAmount();
                        orb.discard();
                        toMarkDirty = true;
                    }
                }
            }
        }

        //fill xp bottles

        ItemStack emptyBottlesStack = blockEntity.inventory.get(SLOT_EMPTY_BOTTLE);
        ItemStack expBottlesStack = blockEntity.inventory.get(SLOT_EXP_BOTTLE);

        if(
                blockEntity.expTank >= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE &&
                        (
                                //A glass bottle to use
                                !emptyBottlesStack.isEmpty() &&
                                        emptyBottlesStack.isOf(Items.GLASS_BOTTLE)
                        )
                        &&
                        (
                                //space in output
                                expBottlesStack.isEmpty() ||
                                        (
                                                expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE) &&
                                                        expBottlesStack.getCount() < expBottlesStack.getMaxCount()
                                        )
                        )
        ){
            blockEntity.xpFillBottleProgress += 1;

            if(blockEntity.xpFillBottleProgress >= blockEntity.xpFillBottleMaxProgress) {
                blockEntity.expTank -= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
                emptyBottlesStack.decrement(1);
                if (expBottlesStack.isEmpty()) {
                    expBottlesStack = new ItemStack(Items.EXPERIENCE_BOTTLE);
                } else {
                    expBottlesStack.increment(1);
                }

                blockEntity.inventory.set(SLOT_EXP_BOTTLE, expBottlesStack);
                blockEntity.xpFillBottleProgress = 0;

                //blockEntity.markDirty(world, pos, state);
                toMarkDirty = true;
            }
        }
        else{
            blockEntity.xpFillBottleProgress = 0;
        }

        if(toMarkDirty){
            blockEntity.markDirty();
        }
    }

    public void commonTick(){
        if(world == null){
            return;
        }

        if(world.getTime() % 20 == 0){
            updateNearbyXp();
        }

        if(xpOrbsNearList != null){
            for(ExperienceOrbEntity orb : xpOrbsNearList){
                Vec3d targetPos = new Vec3d(
                        pos.getX() + 0.5f,
                        pos.getY() + 1f,
                        pos.getZ() + 0.5f
                );
                Vec3d targetDirection = new Vec3d(
                        targetPos.x - orb.getX(),
                        targetPos.y - orb.getY(),
                        targetPos.z - orb.getZ()
                );
                double dis = targetDirection.lengthSquared();
                double e = 1.0 - Math.sqrt(dis) / suckRange;

                if(dis < 1f && targetDirection.y > 0f){
                    targetDirection = targetDirection.add(0f, 0.5f, 0f);

                    if(Math.abs(targetDirection.x) < 0.95f || Math.abs(targetDirection.z) < 0.95f){
                        targetDirection = targetDirection.multiply(-1f, 1f, -1f);
                    }
                }

                orb.setVelocity(orb.getVelocity().add(targetDirection.normalize().multiply(e * e * 0.1)));
            }
        }
    }

    public void updateNearbyXp(){
        if(world == null){
            return;
        }

        float x = pos.getX();
        float y = pos.getY();
        float z = pos.getZ();

        Box box = new Box(
                x - suckRange,
                y - suckRange,
                z - suckRange,
                x + suckRange + 1f,
                y + suckRange + 1f,
                z + suckRange + 1f
        );

        xpOrbsNearList = this.world.getEntitiesByType(TypeFilter.instanceOf(ExperienceOrbEntity.class), box, this::canSuck);
    }

    public boolean canSuck(ExperienceOrbEntity other){
        return true;
    }

    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoExperienceOrbVacuumScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return slot == SLOT_EMPTY_BOTTLE && stack.isOf(Items.GLASS_BOTTLE);
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == SLOT_EXP_BOTTLE;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < inventory.size(); i++){
            if(!inventory.get(i).isEmpty()){
                return false;
            }
        }

        return true;
    }

    /**
     * Fetches the stack currently stored at the given slot. If the slot is empty,
     * or is outside the bounds of this inventory, returns see {@link ItemStack#EMPTY}.
     *
     * @param slot
     */
    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    /**
     * Removes a specific number of items from the given slot.
     *
     * @param slot
     * @param amount
     * @return the removed items as a stack
     */
    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    /**
     * Removes the stack currently stored at the indicated slot.
     *
     * @param slot
     * @return the stack previously stored at the indicated slot.
     */
    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup registryLookup) {
        return createNbt(registryLookup);
    }

    public void dropXP(){
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), expTank);
    }
}