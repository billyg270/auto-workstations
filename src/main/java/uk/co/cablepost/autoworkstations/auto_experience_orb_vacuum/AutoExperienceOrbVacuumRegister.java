package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableScreen;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold.GoldAutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold.GoldAutoExperienceOrbVacuumBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron.IronAutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron.IronAutoExperienceOrbVacuumBlockEntity;

public class AutoExperienceOrbVacuumRegister {
    public static final Identifier AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_experience_orb_vacuum");
    public static final ScreenHandlerType<AutoExperienceOrbVacuumScreenHandler> AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER = new ScreenHandlerType<>(AutoExperienceOrbVacuumScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoExperienceOrbVacuumBlock IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK = new IronAutoExperienceOrbVacuumBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_experience_orb_vacuum");
    public static BlockEntityType<IronAutoExperienceOrbVacuumBlockEntity> IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY;

    //gold
    public static AutoExperienceOrbVacuumBlock GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK = new GoldAutoExperienceOrbVacuumBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_experience_orb_vacuum");
    public static BlockEntityType<GoldAutoExperienceOrbVacuumBlockEntity> GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY;

    public static void onInitialize() {
        //iron

        IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoExperienceOrbVacuumBlockEntity::new,
                        IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, new BlockItem(IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK, new Item.Settings()));

        //gold

        GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoExperienceOrbVacuumBlockEntity::new,
                        GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, new BlockItem(GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        HandledScreens.register(AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER, AutoExperienceOrbVacuumScreen::new);
    }
}
