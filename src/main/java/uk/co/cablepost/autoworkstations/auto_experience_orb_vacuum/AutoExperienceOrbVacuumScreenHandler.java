package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoExperienceOrbVacuumScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public AutoExperienceOrbVacuumScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(2), new ArrayPropertyDelegate(AutoExperienceOrbVacuumBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    public AutoExperienceOrbVacuumScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(AutoExperienceOrbVacuumRegister.AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        //input empty glass bottle
        this.addSlot(new Slot(inventory, AutoExperienceOrbVacuumBlockEntity.SLOT_EMPTY_BOTTLE, 80, 19));

        //output xp bottle
        this.addSlot(new Slot(inventory, AutoExperienceOrbVacuumBlockEntity.SLOT_EXP_BOTTLE, 80, 73));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 102 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 160));
        }
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack quickMove(PlayerEntity player, int invSlot) {
        //TODO - make only go into input slots (9 - 17)

        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStackNoCallbacks(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }



    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    public int getXpFillProgress(){
        int xpFillBottleProgress = this.propertyDelegate.get(0);
        int xpFillBottleMaxProgress = this.propertyDelegate.get(1);

        if(xpFillBottleMaxProgress == 0){
            return 0;
        }

        return 26 * xpFillBottleProgress / xpFillBottleMaxProgress;
    }

    public float getXpBottlesCouldFill(){
        int expTankInt = this.propertyDelegate.get(2);

        float value = expTankInt / AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
        return Math.round(value * 100f) / 100f;
    }
}
