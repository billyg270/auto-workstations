package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;

public class GoldAutoExperienceOrbVacuumBlockEntity extends AutoExperienceOrbVacuumBlockEntity {
    public GoldAutoExperienceOrbVacuumBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoExperienceOrbVacuumRegister.GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 10;
        suckRange = 10f;
    }
}
