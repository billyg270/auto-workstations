package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold.GoldAutoExperienceOrbVacuumBlock;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoExperienceOrbVacuumBlock extends AutoExperienceOrbVacuumBlock {
    public static final MapCodec<IronAutoExperienceOrbVacuumBlock> CODEC = createCodec(IronAutoExperienceOrbVacuumBlock::new);
    public IronAutoExperienceOrbVacuumBlock(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoExperienceOrbVacuumBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                IronAutoExperienceOrbVacuumBlock.validateTicker(type, AutoExperienceOrbVacuumRegister.IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, IronAutoExperienceOrbVacuumBlockEntity::clientTick) :
                IronAutoExperienceOrbVacuumBlock.validateTicker(type, AutoExperienceOrbVacuumRegister.IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, IronAutoExperienceOrbVacuumBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_experience_orb_vacuum.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
