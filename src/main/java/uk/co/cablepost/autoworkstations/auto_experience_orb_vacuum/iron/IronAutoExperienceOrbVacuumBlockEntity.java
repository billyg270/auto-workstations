package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;

public class IronAutoExperienceOrbVacuumBlockEntity extends AutoExperienceOrbVacuumBlockEntity {
    public IronAutoExperienceOrbVacuumBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoExperienceOrbVacuumRegister.IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 20;
        suckRange = 5f;
    }
}
