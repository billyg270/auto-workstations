package uk.co.cablepost.autoworkstations.auto_furnace;

import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.recipe.*;
import net.minecraft.recipe.input.SingleStackRecipeInput;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoFurnaceBlockEntity extends LockableContainerBlockEntity implements SidedInventory, RecipeInputProvider {
    private static final int[] TOP_SLOTS = new int[]{0};
    private static final int[] BOTTOM_SLOTS = new int[]{2, 1, 4};
    private static final int[] SIDE_SLOTS = new int[]{1, 3};
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(5, ItemStack.EMPTY);
    int burnTime;
    int fuelTime;
    int cookTime;

    public int cookSpeedMul = 1;

    int cookTimeTotal;

    int xpFillBottleProgress;
    public int xpFillBottleMaxProgress;
    float expTank;
    private final RecipeType<? extends AbstractCookingRecipe> recipeType = RecipeType.SMELTING;
    private final RecipeManager.MatchGetter<SingleStackRecipeInput, ? extends AbstractCookingRecipe> matchGetter;

    public AutoFurnaceBlockEntity(BlockEntityType<?> blockEntityType, BlockPos pos, BlockState state) {
        super(blockEntityType, pos, state);
        xpFillBottleMaxProgress = 20;
        this.matchGetter = RecipeManager.createCachedMatchGetter(recipeType);
    }

    private boolean isBurning() {
        return burnTime > 0;
    }

    @Override
    public void readNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(nbt, wrapperLookup);
        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory, wrapperLookup);
        burnTime = nbt.getShort("BurnTime");
        cookTime = nbt.getShort("CookTime");
        cookSpeedMul = nbt.getShort("CookSpeedMul");
        cookTimeTotal = nbt.getShort("CookTimeTotal");
        xpFillBottleProgress = nbt.getInt("XpFillBottleProgress");
        xpFillBottleMaxProgress = nbt.getInt("XpFillBottleMaxProgress");
        if(xpFillBottleMaxProgress == 0){
            xpFillBottleMaxProgress = 20;
        }
        fuelTime = getFuelTime(inventory.get(1));
        expTank = nbt.getFloat("ExpTank");
    }

    @Override
    protected void writeNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.writeNbt(nbt, wrapperLookup);
        nbt.putShort("BurnTime", (short)burnTime);
        nbt.putShort("CookTime", (short)cookTime);
        nbt.putShort("CookSpeedMul", (short)cookSpeedMul);
        nbt.putShort("CookTimeTotal", (short)cookTimeTotal);
        nbt.putInt("XpFillBottleProgress", xpFillBottleProgress);
        nbt.putInt("XpFillBottleMaxProgress", xpFillBottleMaxProgress);
        nbt.putFloat("ExpTank", expTank);
        Inventories.writeNbt(nbt, inventory, wrapperLookup);
    }

    public static int PROPERTY_DELEGATE_SIZE = 8;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            return switch (index) {
                case 0 -> AutoFurnaceBlockEntity.this.burnTime;
                case 1 -> AutoFurnaceBlockEntity.this.fuelTime;
                case 2 -> AutoFurnaceBlockEntity.this.cookTime;
                case 3 -> AutoFurnaceBlockEntity.this.cookTimeTotal;
                case 4 -> AutoFurnaceBlockEntity.this.xpFillBottleProgress;
                case 5 -> AutoFurnaceBlockEntity.this.xpFillBottleMaxProgress;
                case 6 -> (int) Math.floor(AutoFurnaceBlockEntity.this.expTank);
                case 7 -> (int) Math.floor((AutoFurnaceBlockEntity.this.expTank % 1) * 100);
                default -> 0;
            };
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };



    @Override
    protected Text getContainerName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    protected DefaultedList<ItemStack> getHeldStacks() {
        return this.inventory;
    }

    @Override
    protected void setHeldStacks(DefaultedList<ItemStack> inventory) {
        this.inventory = inventory;
    }

    @Override
    protected ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return new AutoFurnaceScreenHandler(syncId, playerInventory, this, propertyDelegate);
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoFurnaceBlockEntity blockEntity) {

    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoFurnaceBlockEntity blockEntity) {
        boolean burning = blockEntity.isBurning();
        boolean toMarkDirty = false;
        if (blockEntity.isBurning()) {
            --blockEntity.burnTime;
        }
        ItemStack fuelItemStack = blockEntity.inventory.get(1);
        if (blockEntity.isBurning() || !fuelItemStack.isEmpty() && !blockEntity.inventory.get(0).isEmpty()) {
            RecipeEntry<? extends AbstractCookingRecipe> recipeEntry = blockEntity.matchGetter.getFirstMatch(new SingleStackRecipeInput(blockEntity.inventory.get(0)), world).orElse(null);

            int i = blockEntity.getMaxCountPerStack();
            if (!blockEntity.isBurning()) {
                if (recipeEntry != null && AutoFurnaceBlockEntity.canAcceptRecipeOutput(world.getRegistryManager(), recipeEntry.value(), blockEntity.inventory, i)) {
                    blockEntity.fuelTime = blockEntity.burnTime = blockEntity.getFuelTime(fuelItemStack);
                    if (blockEntity.isBurning()) {
                        toMarkDirty = true;
                        if (!fuelItemStack.isEmpty()) {
                            Item item = fuelItemStack.getItem();
                            fuelItemStack.decrement(1);
                            if (fuelItemStack.isEmpty()) {
                                Item item2 = item.getRecipeRemainder();
                                blockEntity.inventory.set(1, item2 == null ? ItemStack.EMPTY : new ItemStack(item2));
                            }
                        }
                    }
                }
            }
            if (blockEntity.isBurning() && recipeEntry != null && AutoFurnaceBlockEntity.canAcceptRecipeOutput(world.getRegistryManager(), recipeEntry.value(), blockEntity.inventory, i)) {
                blockEntity.cookTime += blockEntity.cookSpeedMul;
                if (blockEntity.cookTime >= blockEntity.cookTimeTotal) {
                    blockEntity.cookTime = 0;
                    blockEntity.cookTimeTotal = AutoFurnaceBlockEntity.getCookTime(world, blockEntity.recipeType, blockEntity);
                    if (AutoFurnaceBlockEntity.craftRecipe(world.getRegistryManager(), recipeEntry.value(), blockEntity.inventory, i)) {
                        blockEntity.expTank += recipeEntry.value().getExperience();
                    }
                    toMarkDirty = true;
                }
            } else {
                blockEntity.cookTime = 0;
            }
        } else if (!blockEntity.isBurning() && blockEntity.cookTime > 0) {
            blockEntity.cookTime = MathHelper.clamp(blockEntity.cookTime - 2, 0, blockEntity.cookTimeTotal);
        }
        if (burning != blockEntity.isBurning()) {//no longer burning
            toMarkDirty = true;
            state = state.with(AbstractFurnaceBlock.LIT, blockEntity.isBurning());
            world.setBlockState(pos, state, Block.NOTIFY_ALL);
        }

        //fill xp bottles

        ItemStack emptyBottlesStack = blockEntity.inventory.get(3);
        ItemStack expBottlesStack = blockEntity.inventory.get(4);

        if(
            blockEntity.expTank >= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE &&
            (
                //A glass bottle to use
                !emptyBottlesStack.isEmpty() &&
                emptyBottlesStack.isOf(Items.GLASS_BOTTLE)
            )
            &&
            (
                //space in output
                expBottlesStack.isEmpty() ||
                (
                    expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE) &&
                    expBottlesStack.getCount() < expBottlesStack.getMaxCount()
                )
            )
        ){
            blockEntity.xpFillBottleProgress += 1;

            if(blockEntity.xpFillBottleProgress >= blockEntity.xpFillBottleMaxProgress) {
                blockEntity.expTank -= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
                emptyBottlesStack.decrement(1);
                if (expBottlesStack.isEmpty()) {
                    expBottlesStack = new ItemStack(Items.EXPERIENCE_BOTTLE);
                } else {
                    expBottlesStack.increment(1);
                }

                blockEntity.inventory.set(4, expBottlesStack);
                blockEntity.xpFillBottleProgress = 0;

                toMarkDirty = true;
            }
        }
        else{
            blockEntity.xpFillBottleProgress = 0;
        }

        if (toMarkDirty) {
            AutoFurnaceBlockEntity.markDirty(world, pos, state);
        }
    }

    private static boolean canAcceptRecipeOutput(DynamicRegistryManager registryManager, @Nullable Recipe<?> recipe, DefaultedList<ItemStack> slots, int count) {
        if (slots.get(0).isEmpty() || recipe == null) {
            return false;
        }
        ItemStack itemStack = recipe.getResult(registryManager);
        if (itemStack.isEmpty()) {
            return false;
        }
        ItemStack itemStack2 = slots.get(2);
        if (itemStack2.isEmpty()) {
            return true;
        }
        if (itemStack2.getItem() != itemStack.getItem()) {
            return false;
        }
        if (itemStack2.getCount() < count && itemStack2.getCount() < itemStack2.getMaxCount()) {
            return true;
        }
        return itemStack2.getCount() < itemStack.getMaxCount();
    }

    private static boolean craftRecipe(DynamicRegistryManager registryManager, @Nullable Recipe<?> recipe, DefaultedList<ItemStack> slots, int count) {
        if (recipe == null || !AutoFurnaceBlockEntity.canAcceptRecipeOutput(registryManager, recipe, slots, count)) {
            return false;
        }
        ItemStack itemStack = slots.get(0);
        ItemStack itemStack2 = recipe.getResult(registryManager);
        ItemStack itemStack3 = slots.get(2);
        if (itemStack3.isEmpty()) {
            slots.set(2, itemStack2.copy());
        } else if (itemStack3.isOf(itemStack2.getItem())) {
            itemStack3.increment(1);
        }
        if (itemStack.isOf(Blocks.WET_SPONGE.asItem()) && !slots.get(1).isEmpty() && slots.get(1).isOf(Items.BUCKET)) {
            slots.set(1, new ItemStack(Items.WATER_BUCKET));
        }
        itemStack.decrement(1);
        return true;
    }

    protected int getFuelTime(ItemStack fuel) {
        if (fuel.isEmpty()) {
            return 0;
        }
        Integer fuelTime = FuelRegistry.INSTANCE.get(fuel.getItem());
        return fuelTime == null ? 0 : fuelTime;
    }

    private static int getCookTime(World world, RecipeType<? extends AbstractCookingRecipe> recipeType, AutoFurnaceBlockEntity inventory) {
        SingleStackRecipeInput singleStackRecipeInput = new SingleStackRecipeInput(inventory.getStack(0));
        return inventory.matchGetter.getFirstMatch(singleStackRecipeInput, world).map((recipe) -> recipe.value().getCookingTime()).orElse(200);
    }

    public static boolean canUseAsFuel(ItemStack stack) {
        return FuelRegistry.INSTANCE.get(stack.getItem()) != null;
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        if (side == Direction.DOWN) {
            return BOTTOM_SLOTS;
        }
        if (side == Direction.UP) {
            return TOP_SLOTS;
        }
        return SIDE_SLOTS;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return this.isValid(slot, stack);
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        if (dir == Direction.DOWN && slot == 1) {
            return stack.isOf(Items.WATER_BUCKET) || stack.isOf(Items.BUCKET);
        }
        return true;
    }

    @Override
    public int size() {
        return this.inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : inventory) {
            if (itemStack.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        return Inventories.splitStack(inventory, slot, amount);
    }

    @Override
    public ItemStack removeStack(int slot) {
        return Inventories.removeStack(inventory, slot);
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        ItemStack itemStack = inventory.get(slot);
        boolean bl = !stack.isEmpty() && !itemStack.isEmpty() && ItemStack.areItemsAndComponentsEqual(stack, itemStack);
        inventory.set(slot, stack);
        if (stack.getCount() > getMaxCountPerStack()) {
            stack.setCount(getMaxCountPerStack());
        }
        if (slot == 0 && !bl) {
            assert world != null;
            cookTimeTotal = AutoFurnaceBlockEntity.getCookTime(world, recipeType, this);
            cookTime = 0;
            markDirty();
        }
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        assert world != null;
        if (world.getBlockEntity(pos) != this) {
            return false;
        }
        return player.squaredDistanceTo((double)pos.getX() + 0.5, (double)pos.getY() + 0.5, (double)pos.getZ() + 0.5) <= 64.0;
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        if (slot == 2 || slot == 5) {
            return false;
        }
        if (slot == 1) {
            ItemStack itemStack = inventory.get(1);
            return AutoFurnaceBlockEntity.canUseAsFuel(stack) || stack.isOf(Items.BUCKET) && !itemStack.isOf(Items.BUCKET);
        }
        if (slot == 3) {
            return stack.isOf(Items.GLASS_BOTTLE);
        }
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void provideRecipeInputs(RecipeMatcher finder) {
        for (ItemStack itemStack : inventory) {
            finder.addInput(itemStack);
        }
    }

    public void dropXP(){
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), (int)Math.floor(expTank));
    }
}
