package uk.co.cablepost.autoworkstations.auto_furnace;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_furnace.gold.GoldAutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.gold.GoldAutoFurnaceBlockEntity;
import uk.co.cablepost.autoworkstations.auto_furnace.iron.IronAutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.iron.IronAutoFurnaceBlockEntity;

public class AutoFurnaceRegister {
    //screen
    public static final Identifier AUTO_FURNACE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_furnace");
    public static final ScreenHandlerType<AutoFurnaceScreenHandler> AUTO_FURNACE_SCREEN_HANDLER = new ScreenHandlerType<>(AutoFurnaceScreenHandler::new, FeatureSet.empty());

    //iron
    public static AutoFurnaceBlock IRON_AUTO_FURNACE_BLOCK = new IronAutoFurnaceBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_FURNACE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_furnace");
    public static BlockEntityType<IronAutoFurnaceBlockEntity> IRON_AUTO_FURNACE_BLOCK_ENTITY;

    //gold
    public static AutoFurnaceBlock GOLD_AUTO_FURNACE_BLOCK = new GoldAutoFurnaceBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_FURNACE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_furnace");
    public static BlockEntityType<GoldAutoFurnaceBlockEntity> GOLD_AUTO_FURNACE_BLOCK_ENTITY;

    public static void onInitialize(){
        //iron

        IRON_AUTO_FURNACE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_FURNACE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoFurnaceBlockEntity::new,
                        IRON_AUTO_FURNACE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_FURNACE_IDENTIFIER, IRON_AUTO_FURNACE_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_FURNACE_IDENTIFIER, new BlockItem(IRON_AUTO_FURNACE_BLOCK, new Item.Settings()));

        //gold

        GOLD_AUTO_FURNACE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_FURNACE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoFurnaceBlockEntity::new,
                        GOLD_AUTO_FURNACE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_FURNACE_IDENTIFIER, GOLD_AUTO_FURNACE_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_FURNACE_IDENTIFIER, new BlockItem(GOLD_AUTO_FURNACE_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_FURNACE_IDENTIFIER, AUTO_FURNACE_SCREEN_HANDLER);
    }

    public static void onInitializeClient(){
        HandledScreens.register(AUTO_FURNACE_SCREEN_HANDLER, AutoFurnaceScreen::new);
    }
}
