package uk.co.cablepost.autoworkstations.auto_furnace.gold;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron.IronAutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class GoldAutoFurnaceBlock extends AutoFurnaceBlock {
    public static final MapCodec<GoldAutoFurnaceBlock> CODEC = createCodec(GoldAutoFurnaceBlock::new);
    public GoldAutoFurnaceBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldAutoFurnaceBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, AutoFurnaceRegister.GOLD_AUTO_FURNACE_BLOCK_ENTITY, GoldAutoFurnaceBlockEntity::clientTick) : validateTicker(type, AutoFurnaceRegister.GOLD_AUTO_FURNACE_BLOCK_ENTITY, GoldAutoFurnaceBlockEntity::serverTick);
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.gold_auto_furnace.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
