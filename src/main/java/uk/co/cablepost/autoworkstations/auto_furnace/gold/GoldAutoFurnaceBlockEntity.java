package uk.co.cablepost.autoworkstations.auto_furnace.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlockEntity;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;

public class GoldAutoFurnaceBlockEntity extends AutoFurnaceBlockEntity {
    public GoldAutoFurnaceBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoFurnaceRegister.GOLD_AUTO_FURNACE_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 10;
        cookSpeedMul = 2;
    }
}
