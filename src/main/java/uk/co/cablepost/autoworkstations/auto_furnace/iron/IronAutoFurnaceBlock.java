package uk.co.cablepost.autoworkstations.auto_furnace.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.tooltip.TooltipType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoFurnaceBlock extends AutoFurnaceBlock {
    public static final MapCodec<IronAutoFurnaceBlock> CODEC = createCodec(IronAutoFurnaceBlock::new);
    public IronAutoFurnaceBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoFurnaceBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, AutoFurnaceRegister.IRON_AUTO_FURNACE_BLOCK_ENTITY, IronAutoFurnaceBlockEntity::clientTick) : validateTicker(type, AutoFurnaceRegister.IRON_AUTO_FURNACE_BLOCK_ENTITY, IronAutoFurnaceBlockEntity::serverTick);
    }

    @Override
    public void appendTooltip(ItemStack stack, Item.TooltipContext context, List<Text> tooltip, TooltipType options) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_furnace.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
