package uk.co.cablepost.autoworkstations.auto_grindstone;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.ItemEnchantmentsComponent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.registry.entry.RegistryEntry;
import net.minecraft.registry.tag.EnchantmentTags;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import java.util.Objects;
import java.util.Random;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoGrindstoneBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {

    public static int INVENTORY_SIZE = 5;
    public static int SLOT_EXP_BOTTLE = 0;
    public static int SLOT_EMPTY_BOTTLE = 1;
    public static int SLOT_ITEM_IN = 2;
    public static int SLOT_ITEM_ADD = 3;
    public static int SLOT_ITEM_OUT = 4;

    public static int MODE_GRIND = 0;
    public static int MODE_DISENCHANT = 1;

    public static int PROPERTY_DELEGATE_SIZE = 7;

    public int xpFillBottleMaxProgress;
    public int processProgress;
    public int maxProcessProgress;

    public int mode = MODE_GRIND;

    public ItemStack result = null;
    public ItemStack lastInputStack = null;
    public ItemStack lastAdditionStack = null;
    public int lastMode = -1;
    public Random random;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);
    int xpFillBottleProgress;
    float expTank;
    protected final PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            switch (index) {
                case 0 -> {
                    return AutoGrindstoneBlockEntity.this.xpFillBottleProgress;
                }
                case 1 -> {
                    return AutoGrindstoneBlockEntity.this.xpFillBottleMaxProgress;
                }
                case 2 -> {
                    return (int) Math.floor(AutoGrindstoneBlockEntity.this.expTank);
                }
                case 3 -> {
                    return (int) Math.floor((AutoGrindstoneBlockEntity.this.expTank % 1) * 100);
                }
                case 4 -> {
                    return AutoGrindstoneBlockEntity.this.processProgress;
                }
                case 5 -> {
                    return AutoGrindstoneBlockEntity.this.maxProcessProgress;
                }
                case 6 -> {
                    return AutoGrindstoneBlockEntity.this.mode;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            if (index == 6) {
                AutoGrindstoneBlockEntity.this.updateResult();
                AutoGrindstoneBlockEntity.this.mode = value;
            }
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public AutoGrindstoneBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        xpFillBottleMaxProgress = 20;
        maxProcessProgress = 60;
        random = new Random();
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoGrindstoneBlockEntity blockEntity) {
        if (blockEntity.processProgress > 0) {
            blockEntity.processProgress++;
        }
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoGrindstoneBlockEntity blockEntity) {
        boolean toMarkDirty = false;

        //empty xp bottles
        //fill xp bottles

        ItemStack emptyBottlesStack = blockEntity.inventory.get(SLOT_EMPTY_BOTTLE);
        ItemStack expBottlesStack = blockEntity.inventory.get(SLOT_EXP_BOTTLE);

        if (
                blockEntity.expTank >= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE &&
                        (
                                //A glass bottle to use
                                !emptyBottlesStack.isEmpty() &&
                                        emptyBottlesStack.isOf(Items.GLASS_BOTTLE)
                        )
                        &&
                        (
                                //space in output
                                expBottlesStack.isEmpty() ||
                                        (
                                                expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE) &&
                                                        expBottlesStack.getCount() < expBottlesStack.getMaxCount()
                                        )
                        )
        ) {
            blockEntity.xpFillBottleProgress += 1;

            if (blockEntity.xpFillBottleProgress >= blockEntity.xpFillBottleMaxProgress) {
                blockEntity.expTank -= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
                emptyBottlesStack.decrement(1);
                if (expBottlesStack.isEmpty()) {
                    expBottlesStack = new ItemStack(Items.EXPERIENCE_BOTTLE);
                } else {
                    expBottlesStack.increment(1);
                }

                blockEntity.inventory.set(SLOT_EXP_BOTTLE, expBottlesStack);
                blockEntity.xpFillBottleProgress = 0;

                toMarkDirty = true;
            }
        } else {
            blockEntity.xpFillBottleProgress = 0;
        }

        //--- grindstone stuff ---

        ItemStack input = blockEntity.getStack(SLOT_ITEM_IN);
        ItemStack addition = blockEntity.getStack(SLOT_ITEM_ADD);

        if (
                blockEntity.lastMode != blockEntity.mode ||
                        blockEntity.lastInputStack == null ||
                        blockEntity.lastInputStack.isEmpty() != input.isEmpty() ||
                        blockEntity.lastInputStack.getItem() != input.getItem() ||
                        blockEntity.lastInputStack.getCount() != input.getCount() ||
                        blockEntity.lastAdditionStack == null ||
                        blockEntity.lastAdditionStack.isEmpty() != addition.isEmpty() ||
                        blockEntity.lastAdditionStack.getItem() != addition.getItem() ||
                        blockEntity.lastAdditionStack.getCount() != addition.getCount() ||
                        blockEntity.result == null
        ) {
            blockEntity.lastInputStack = input.copy();
            blockEntity.lastAdditionStack = addition.copy();

            blockEntity.lastMode = blockEntity.mode;

            ItemStack resultBefore = blockEntity.result == null ? null : blockEntity.result.copy();
            blockEntity.updateResult();
            if (
                    resultBefore == null ||
                            blockEntity.result.isEmpty() != resultBefore.isEmpty() ||
                            blockEntity.result.getItem() != resultBefore.getItem() ||
                            resultBefore.getDamage() != blockEntity.result.getDamage() ||
                            !Objects.equals(resultBefore.get(DataComponentTypes.REPAIR_COST), blockEntity.result.get(DataComponentTypes.REPAIR_COST))
            ) {
                blockEntity.processProgress = 0;
            }
        }

        if (!blockEntity.result.isEmpty() &&
                blockEntity.getStack(SLOT_ITEM_OUT).isEmpty()) {
            blockEntity.processProgress++;
            if (blockEntity.processProgress == 1) {//Client will do the rest
                toMarkDirty = true;
            }

            if (blockEntity.processProgress == blockEntity.maxProcessProgress - 15) {
                world.playSound(null, pos, SoundEvents.BLOCK_GRINDSTONE_USE, SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
            }

            if (blockEntity.processProgress >= blockEntity.maxProcessProgress) {
                blockEntity.doProcess();
                blockEntity.processProgress = 0;
                toMarkDirty = true;
            }
        } else {
            blockEntity.processProgress = 0;
        }

        //--- ---

        if (toMarkDirty) {
            blockEntity.markDirty();
        }
    }

    @Override
    public void readNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(nbt, wrapperLookup);

        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory, wrapperLookup);

        xpFillBottleProgress = nbt.getInt("XpFillBottleProgress");
        xpFillBottleMaxProgress = nbt.getInt("XpFillBottleMaxProgress");
        if (xpFillBottleMaxProgress == 0) {
            xpFillBottleMaxProgress = 20;
        }
        expTank = nbt.getFloat("ExpTank");

        processProgress = nbt.getInt("ProcessProgress");
        maxProcessProgress = nbt.getInt("MaxProcessProgress");
        if (maxProcessProgress == 0) {
            maxProcessProgress = 60;
        }

        mode = nbt.getInt("Mode");
    }

    @Override
    protected void writeNbt(NbtCompound nbt, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.writeNbt(nbt, wrapperLookup);

        Inventories.writeNbt(nbt, inventory, wrapperLookup);

        nbt.putInt("XpFillBottleProgress", xpFillBottleProgress);
        nbt.putInt("XpFillBottleMaxProgress", xpFillBottleMaxProgress);
        nbt.putFloat("ExpTank", expTank);

        nbt.putInt("ProcessProgress", processProgress);
        nbt.putInt("MaxProcessProgress", maxProcessProgress);

        nbt.putInt("Mode", mode);
    }

    public void doProcess() {
        addExperienceLevels();

        setStack(SLOT_ITEM_IN, ItemStack.EMPTY);
        setStack(SLOT_ITEM_ADD, ItemStack.EMPTY);

        setStack(SLOT_ITEM_OUT, result.copy());
    }

    /**
     * Returns the title of this screen handler; will be a part of the open
     * screen packet sent to the client.
     */
    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoGrindstoneScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if (dir == Direction.UP || dir == Direction.DOWN) {
            return slot == SLOT_ITEM_IN && getStack(slot).isEmpty();
        }

        if (stack.isOf(Items.GLASS_BOTTLE)) {
            return slot == SLOT_EMPTY_BOTTLE;
        }

        if (stack.isOf(Items.EXPERIENCE_BOTTLE)) {
            return slot == SLOT_EXP_BOTTLE;
        }

        return slot == SLOT_ITEM_ADD;//a && getStack(slot).isEmpty();
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == SLOT_EXP_BOTTLE || slot == SLOT_ITEM_OUT;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : inventory) {
            if (!itemStack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Fetches the stack currently stored at the given slot. If the slot is empty,
     * or is outside the bounds of this inventory, returns see {@link ItemStack#EMPTY}.
     *
     * @param slot
     */
    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    /**
     * Removes a specific number of items from the given slot.
     *
     * @param slot
     * @param amount
     * @return the removed items as a stack
     */
    @Override
    public ItemStack removeStack(int slot, int amount) {
        if (amount == 0) {
            return ItemStack.EMPTY;
        }

        if (amount == inventory.get(slot).getCount()) {
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    /**
     * Removes the stack currently stored at the indicated slot.
     * @param slot
     * @return the stack previously stored at the indicated slot.
     */
    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup registryLookup) {
        return createNbt(registryLookup);
    }

    public void addExperienceLevels() {
        this.expTank += this.getExperience(this.getWorld());
        /*this.expTank += recipeEntry.value().getExperience();
        this.expLevel += levels;
        if (this.expLevel < 0) {
            this.expLevel = 0;
            this.expProgress = 0.0f;
        }*/
    }

    private int getExperience(World world) {
        int i = 0;
        i += this.getExperience(this.inventory.get(SLOT_ITEM_IN));
        if ((i += this.getExperience(this.inventory.get(SLOT_ITEM_ADD))) > 0) {
            int j = (int) Math.ceil((double) i / 2.0);
            return j + world.random.nextInt(j);
        }
        return 0;
    }

    private int getExperience(ItemStack stack) {
        int i = 0;
        ItemEnchantmentsComponent itemEnchantmentsComponent = EnchantmentHelper.getEnchantments(stack);
        for (Object2IntMap.Entry<RegistryEntry<Enchantment>> e : itemEnchantmentsComponent.getEnchantmentEntries()) {
            Enchantment enchantment = e.getKey().value();
            int j = e.getIntValue();
            if (!e.getKey().isIn(EnchantmentTags.CURSE)) {
                i += enchantment.getMinPower(j);
            }
        }
        return i;
    }

    public void dropXP() {
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), (int) Math.floor(expTank));
    }

    // ------------------------------------------------------------------------

    /*
     * Enabled aggressive block sorting
     */
    private void updateResult() {
        ItemStack itemStack = this.getStack(SLOT_ITEM_IN);
        ItemStack itemStack2 = this.getStack(SLOT_ITEM_ADD);

        this.result = this.getOutputStack(itemStack, itemStack2, this.mode == MODE_DISENCHANT);
    }

    private ItemStack getOutputStack(ItemStack firstInput, ItemStack secondInput, boolean disenchant) {
        boolean bl = !firstInput.isEmpty() || !secondInput.isEmpty();
        if (!bl) {
            return ItemStack.EMPTY;
        } else if (firstInput.getCount() <= 1 && secondInput.getCount() <= 1) {
            boolean bl2 = !firstInput.isEmpty() && !secondInput.isEmpty();
            if (!bl2 && disenchant) {
                ItemStack itemStack = !firstInput.isEmpty() ? firstInput : secondInput;
                return !EnchantmentHelper.hasEnchantments(itemStack) ? ItemStack.EMPTY : this.grind(itemStack.copy());
            } else {
                return this.combineItems(firstInput, secondInput);
            }
        } else {
            return ItemStack.EMPTY;
        }
    }

    private ItemStack transferEnchantments(ItemStack target, ItemStack source) {
        EnchantmentHelper.apply(target, (components) -> {
            ItemEnchantmentsComponent itemEnchantmentsComponent = EnchantmentHelper.getEnchantments(source);
            for (Object2IntMap.Entry<RegistryEntry<Enchantment>> entry : itemEnchantmentsComponent.getEnchantmentEntries()) {
                var enchantment = entry.getKey();
                if (enchantment.isIn(EnchantmentTags.CURSE) && EnchantmentHelper.getLevel(enchantment, source) != 0) continue;
                components.add(enchantment, entry.getIntValue());
            }
        });
        return target.copy();
    }

    private ItemStack combineItems(ItemStack firstInput, ItemStack secondInput) {
        if (!firstInput.isOf(secondInput.getItem())) {
            return ItemStack.EMPTY;
        } else {
            int i = Math.max(firstInput.getMaxDamage(), secondInput.getMaxDamage());
            int j = firstInput.getMaxDamage() - firstInput.getDamage();
            int k = secondInput.getMaxDamage() - secondInput.getDamage();
            int l = j + k + i * 5 / 100;
            int m = 1;
            if (!firstInput.isDamageable()) {
                if (firstInput.getMaxCount() < 2 || !ItemStack.areEqual(firstInput, secondInput)) {
                    return ItemStack.EMPTY;
                }

                m = 2;
            }

            ItemStack itemStack = firstInput.copyWithCount(m);
            if (itemStack.isDamageable()) {
                itemStack.set(DataComponentTypes.MAX_DAMAGE, i);
                itemStack.setDamage(Math.max(i - l, 0));
            }

            itemStack = this.transferEnchantments(itemStack, secondInput);
            return this.grind(itemStack);
        }
    }

    private ItemStack grind(ItemStack item) {
        ItemEnchantmentsComponent itemEnchantmentsComponent = EnchantmentHelper.apply(item,
                (components) -> components.remove((enchantment) -> !enchantment.isIn(EnchantmentTags.CURSE)));
        if (item.isOf(Items.ENCHANTED_BOOK) && itemEnchantmentsComponent.isEmpty()) {
            item = item.copyComponentsToNewStack(Items.BOOK, item.getCount());
        }

        int i = 0;

        for(int j = 0; j < itemEnchantmentsComponent.getSize(); ++j) {
            i = AnvilScreenHandler.getNextCost(i);
        }

        item.set(DataComponentTypes.REPAIR_COST, i);
        return item;
    }
}
