package uk.co.cablepost.autoworkstations.auto_grindstone;

import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_grindstone.gold.GoldAutoGrindstoneBlock;
import uk.co.cablepost.autoworkstations.auto_grindstone.gold.GoldAutoGrindstoneBlockEntity;
import uk.co.cablepost.autoworkstations.auto_grindstone.iron.IronAutoGrindstoneBlock;
import uk.co.cablepost.autoworkstations.auto_grindstone.iron.IronAutoGrindstoneBlockEntity;

public class AutoGrindstoneRegister {
    public static final CustomPayload.Id<UpdateGrindstonePayload> UPDATE_AUTO_GRINDSTONE_PACKET_ID = new CustomPayload.Id<>(Identifier.of(AutoWorkstations.MOD_ID, "update_grindstone"));

    //screen
    public static final Identifier AUTO_GRINDSTONE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "auto_grindstone");
    public static final Identifier IRON_AUTO_GRINDSTONE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "iron_auto_grindstone");
    public static final ScreenHandlerType<AutoGrindstoneScreenHandler> AUTO_GRINDSTONE_SCREEN_HANDLER = new ScreenHandlerType<>(AutoGrindstoneScreenHandler::new, FeatureSet.empty());
    public static final Identifier GOLD_AUTO_GRINDSTONE_IDENTIFIER = Identifier.of(AutoWorkstations.MOD_ID, "gold_auto_grindstone");
    //iron
    public static AutoGrindstoneBlock IRON_AUTO_GRINDSTONE_BLOCK = new IronAutoGrindstoneBlock(FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(2.0f));
    public static BlockEntityType<IronAutoGrindstoneBlockEntity> IRON_AUTO_GRINDSTONE_BLOCK_ENTITY;

    //gold
    public static AutoGrindstoneBlock GOLD_AUTO_GRINDSTONE_BLOCK = new GoldAutoGrindstoneBlock(FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(1.0f));
    public static BlockEntityType<GoldAutoGrindstoneBlockEntity> GOLD_AUTO_GRINDSTONE_BLOCK_ENTITY;

    public static void onInitialize() {
        PayloadTypeRegistry.playC2S().register(UPDATE_AUTO_GRINDSTONE_PACKET_ID, UpdateGrindstonePayload.CODEC);

        //iron

        IRON_AUTO_GRINDSTONE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_GRINDSTONE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoGrindstoneBlockEntity::new,
                        IRON_AUTO_GRINDSTONE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_GRINDSTONE_IDENTIFIER, IRON_AUTO_GRINDSTONE_BLOCK);
        Registry.register(Registries.ITEM, IRON_AUTO_GRINDSTONE_IDENTIFIER, new BlockItem(IRON_AUTO_GRINDSTONE_BLOCK, new Item.Settings()));

        //gold

        GOLD_AUTO_GRINDSTONE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_GRINDSTONE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoGrindstoneBlockEntity::new,
                        GOLD_AUTO_GRINDSTONE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_GRINDSTONE_IDENTIFIER, GOLD_AUTO_GRINDSTONE_BLOCK);
        Registry.register(Registries.ITEM, GOLD_AUTO_GRINDSTONE_IDENTIFIER, new BlockItem(GOLD_AUTO_GRINDSTONE_BLOCK, new Item.Settings()));

        Registry.register(Registries.SCREEN_HANDLER, AUTO_GRINDSTONE_IDENTIFIER, AUTO_GRINDSTONE_SCREEN_HANDLER);

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_AUTO_GRINDSTONE_PACKET_ID, (payload, context) -> {
            int mode = payload.mode();
            ServerPlayerEntity player = context.player();

            if (!player.isSpectator() && player.currentScreenHandler instanceof AutoGrindstoneScreenHandler screenHandler) {
                screenHandler.setSelectedMode(player, mode);
            }
        });
    }

    public static void onInitializeClient() {
        HandledScreens.register(AUTO_GRINDSTONE_SCREEN_HANDLER, AutoGrindstoneScreen::new);
    }


    public record UpdateGrindstonePayload(int mode) implements CustomPayload {
        public static final PacketCodec<RegistryByteBuf, UpdateGrindstonePayload> CODEC = CustomPayload.codecOf(UpdateGrindstonePayload::write, UpdateGrindstonePayload::fromBuf);

        private static UpdateGrindstonePayload fromBuf(RegistryByteBuf buf) {
            return new UpdateGrindstonePayload(buf.readVarInt());
        }

        private void write(RegistryByteBuf buf) {
            buf.writeVarInt(this.mode);
        }

        @Override
        public Id<? extends CustomPayload> getId() {
            return AutoGrindstoneRegister.UPDATE_AUTO_GRINDSTONE_PACKET_ID;
        }
    }
}
