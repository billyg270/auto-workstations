package uk.co.cablepost.autoworkstations.auto_grindstone;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;

public class AutoGrindstoneScreen extends HandledScreen<AutoGrindstoneScreenHandler> {
    private static final Identifier TEXTURE = Identifier.of(AutoWorkstations.MOD_ID, "textures/gui/container/auto_grindstone.png");

    public AutoGrindstoneScreen(AutoGrindstoneScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.titleY = 4;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //process progress
        int processProgress = handler.getProcessProgress();
        context.drawTexture(TEXTURE, x + 59, y + 54, 176, 14, processProgress + 1, 17);

        //Mode Select
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_grindstone.mode.grind"), x + 69, y + 15, 4210752, false);
        context.drawText(this.textRenderer, Text.translatable("gui.autoworkstations.auto_grindstone.mode.disenchant"), x + 118, y + 15, 4210752, false);

        int mode = handler.getSelectedMode();
        if (mode == AutoGrindstoneBlockEntity.MODE_GRIND) {
            context.drawText(this.textRenderer, Text.of("x"), x + 60, y + 14, 0xffffff, false);
        }
        if (mode == AutoGrindstoneBlockEntity.MODE_DISENCHANT) {
            context.drawText(this.textRenderer, Text.of("x"), x + 109, y + 14, 0xffffff, false);
        }

        //xp level
        int xpFillProgress = handler.getXpFillProgress();
        context.drawTexture(TEXTURE, x + 133, y + 49, 176, 55, 18, xpFillProgress);

        context.drawText(this.textRenderer, Text.of("x" + handler.getXpBottlesCouldFill()), x + 126, y + 50, 0x404040, false);
        context.drawText(this.textRenderer, Text.of("x" + handler.getXpBottlesCouldFill()), x + 125, y + 49, 0x33de00, false);


    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(context, mouseX, mouseY);
        if (this.getScreenHandler().getSelectedMode() == AutoGrindstoneBlockEntity.MODE_DISENCHANT) {
            context.drawTexture(TEXTURE, x + 27, y + 55, 176, 35, 18, 18);
        }
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (client == null || client.player == null || client.player.isSpectator()) {
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        if (mouseY >= y + 15 && mouseY <= y + 21) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                ClientPlayNetworking.send(new AutoGrindstoneRegister.UpdateGrindstonePayload(AutoGrindstoneBlockEntity.MODE_GRIND));
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                ClientPlayNetworking.send(new AutoGrindstoneRegister.UpdateGrindstonePayload(AutoGrindstoneBlockEntity.MODE_DISENCHANT));
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    protected void drawForeground(DrawContext context, int mouseX, int mouseY) {
        context.drawText(this.textRenderer, this.title, this.titleX, this.titleY, 4210752, false);

        if (mouseY >= y + 15 && mouseY <= y + 21) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                //disenchant
                context.drawTooltip(
                        this.textRenderer,
                        Text.translatable("gui.autoworkstations.auto_grindstone.mode.disenchant.tooltip"),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                //grind
                context.drawTooltip(
                        this.textRenderer,
                        Text.translatable("gui.autoworkstations.auto_grindstone.mode.grind.tooltip"),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
        }
    }
}
