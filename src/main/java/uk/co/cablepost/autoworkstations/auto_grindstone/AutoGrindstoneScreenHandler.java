package uk.co.cablepost.autoworkstations.auto_grindstone;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.GlassBottleItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.server.network.ServerPlayerEntity;
import uk.co.cablepost.autoworkstations.AutoWorkstations;


public class AutoGrindstoneScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public AutoGrindstoneScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(AutoGrindstoneBlockEntity.INVENTORY_SIZE), new ArrayPropertyDelegate(AutoGrindstoneBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    protected AutoGrindstoneScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(AutoGrindstoneRegister.AUTO_GRINDSTONE_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        this.addSlot(new Slot(inventory, AutoGrindstoneBlockEntity.SLOT_EMPTY_BOTTLE, 124, 27) {
            @Override
            public boolean canInsert(ItemStack stack) {
                return !stack.isEmpty() && stack.getItem() instanceof GlassBottleItem;
            }
        });

        this.addSlot(new Slot(inventory, AutoGrindstoneBlockEntity.SLOT_EXP_BOTTLE, 124, 81) {
            @Override
            public boolean canInsert(ItemStack stack) {
                return false;
            }
        });

        this.addSlot(new Slot(inventory, AutoGrindstoneBlockEntity.SLOT_ITEM_IN, 28, 32) {

            @Override
            public boolean canInsert(ItemStack stack) {
                return stack.isDamageable() || stack.isOf(Items.ENCHANTED_BOOK) || stack.hasEnchantments();
            }
        });

        this.addSlot(new Slot(inventory, AutoGrindstoneBlockEntity.SLOT_ITEM_ADD, 28, 56) {

            @Override
            public boolean isEnabled() {
                return propertyDelegate.get(6) != AutoGrindstoneBlockEntity.MODE_DISENCHANT && super.isEnabled();
            }

            @Override
            public boolean canInsert(ItemStack stack) {
                return stack.isDamageable();
            }
        });

        this.addSlot(new Slot(inventory, AutoGrindstoneBlockEntity.SLOT_ITEM_OUT, 95, 54) {
            @Override
            public boolean canInsert(ItemStack stack) {
                return false;
            }
        });

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 102 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 160));
        }
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack quickMove(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStackNoCallbacks(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    public int getXpFillProgress() {
        int xpFillBottleProgress = this.propertyDelegate.get(0);
        int xpFillBottleMaxProgress = this.propertyDelegate.get(1);

        if (xpFillBottleMaxProgress == 0) {
            return 0;
        }

        return 26 * xpFillBottleProgress / xpFillBottleMaxProgress;
    }

    public float getXpBottlesCouldFill() {
        int expTankInt = this.propertyDelegate.get(2);
        int expTankDec100 = this.propertyDelegate.get(3);

        float value = (expTankInt + ((float) expTankDec100 / 100f)) / AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
        return Math.round(value * 100f) / 100f;
    }

    public int getProcessProgress() {
        int processProgress = this.propertyDelegate.get(4);
        int maxProcessProgress = this.propertyDelegate.get(5);

        if (maxProcessProgress == 0) {
            return 0;
        }

        return 24 * processProgress / maxProcessProgress;
    }

    public int getSelectedMode() {
        return this.propertyDelegate.get(6);
    }

    public void setSelectedMode(ServerPlayerEntity player, int value) {
        if (this.propertyDelegate.get(6) != value) {
            ItemStack stack = this.inventory.getStack(AutoGrindstoneBlockEntity.SLOT_ITEM_ADD).copy();
            if (!player.getInventory().insertStack(stack)) {
                player.dropItem(stack, false);
            }
            this.inventory.setStack(AutoGrindstoneBlockEntity.SLOT_ITEM_ADD, ItemStack.EMPTY);
        }
        this.propertyDelegate.set(6, value);
    }

    public int getLevelCost() {
        return this.propertyDelegate.get(7);
    }
}