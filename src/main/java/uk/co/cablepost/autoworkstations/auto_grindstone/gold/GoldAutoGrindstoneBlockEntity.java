package uk.co.cablepost.autoworkstations.auto_grindstone.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneBlockEntity;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneRegister;

public class GoldAutoGrindstoneBlockEntity extends AutoGrindstoneBlockEntity {
    public GoldAutoGrindstoneBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoGrindstoneRegister.GOLD_AUTO_GRINDSTONE_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 10;
        maxProcessProgress = 30;
    }
}
