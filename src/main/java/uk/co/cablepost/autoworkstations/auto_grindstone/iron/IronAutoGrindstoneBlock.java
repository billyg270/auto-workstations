package uk.co.cablepost.autoworkstations.auto_grindstone.iron;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneBlock;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneRegister;

public class IronAutoGrindstoneBlock extends AutoGrindstoneBlock {
    public static final MapCodec<IronAutoGrindstoneBlock> CODEC = createCodec(IronAutoGrindstoneBlock::new);

    public IronAutoGrindstoneBlock(Settings settings) {
        super(settings);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return CODEC;
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoGrindstoneBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return
                world.isClient ?
                        IronAutoGrindstoneBlock.validateTicker(type, AutoGrindstoneRegister.IRON_AUTO_GRINDSTONE_BLOCK_ENTITY, IronAutoGrindstoneBlockEntity::clientTick) :
                        IronAutoGrindstoneBlock.validateTicker(type, AutoGrindstoneRegister.IRON_AUTO_GRINDSTONE_BLOCK_ENTITY, IronAutoGrindstoneBlockEntity::serverTick)
                ;
    }
}
