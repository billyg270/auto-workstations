package uk.co.cablepost.autoworkstations.auto_grindstone.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneBlockEntity;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneRegister;

public class IronAutoGrindstoneBlockEntity extends AutoGrindstoneBlockEntity {
    public IronAutoGrindstoneBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoGrindstoneRegister.IRON_AUTO_GRINDSTONE_BLOCK_ENTITY, blockPos, blockState);
        xpFillBottleMaxProgress = 20;
        maxProcessProgress = 60;
    }
}
