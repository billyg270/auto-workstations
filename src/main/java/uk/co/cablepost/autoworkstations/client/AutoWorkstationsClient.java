package uk.co.cablepost.autoworkstations.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilRegister;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandRegister;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableRegister;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterRegister;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumRegister;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceRegister;
import uk.co.cablepost.autoworkstations.auto_grindstone.AutoGrindstoneRegister;

@Environment(EnvType.CLIENT)
public class AutoWorkstationsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        AutoCraftingTableRegister.onInitializeClient();
        AutoFurnaceRegister.onInitializeClient();
        AutoEnchantingTableRegister.onInitializeClient();
        AutoExperienceOrbVacuumRegister.onInitializeClient();
        AutoExperienceOrbEmitterRegister.onInitializeClient();
        AutoAnvilRegister.onInitializeClient();
        AutoGrindstoneRegister.onInitializeClient();
        AutoBrewingStandRegister.onInitializeClient();
    }
}