package uk.co.cablepost.autoworkstations.util;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;

public interface ImplementedInventory extends Inventory, SidedInventory {
    DefaultedList<ItemStack> getItems();

    @Override
    default int size() {
        return getItems().size();
    }

    @Override
    default boolean isEmpty() {
        return getItems()
                .stream()
                .filter(ItemStack::isEmpty)
                .toArray()
                .length > 0;
    }

    @Override
    default ItemStack getStack(int slot) {
        return getItems().get(slot);
    }

    @Override
    default ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == getItems().get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = getItems().get(slot).copy();
        getItems().get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    @Override
    default ItemStack removeStack(int slot) {
        ItemStack toRet = getItems().get(slot).copy();
        getItems().set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    default void setStack(int slot, ItemStack stack) {
        getItems().set(slot, stack);
    }

    @Override
    default void clear() {
        getItems().clear();
    }

    @Override
    default boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    default int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[getItems().size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }
}
